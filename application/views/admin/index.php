<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>  <?php echo getInfo("nama_perusahaan") ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo base_url("template/bootstrap/css/bootstrap.min.css") ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("template/dist/css/AdminLTE.min.css") ?>">

  <link rel="stylesheet" href="<?php echo base_url("template/dist/css/skins/skin-blue.min.css") ?>">

  <link rel="stylesheet" href="<?php echo base_url("template/plugins/datatables/dataTables.bootstrap.css") ?>">

  <link rel="stylesheet" href="<?php echo base_url("template/plugins/datepicker/datepicker3.css") ?>">

  <link rel="stylesheet" href="<?php echo base_url("template/plugins/bootstrap-sweetalert/sweet-alert.css") ?>">



  <!-- jQuery 2.1.4 -->
  <script src="<?php echo base_url("template/plugins/jQuery/jQuery-2.1.4.min.js") ?>"></script>

</head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
  <?php $idAdmin = $this->session->userdata['pengguna']['id_admin']; ?>
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
      <!-- Logo -->
      <a href="<?php echo base_url(); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
          <img style="height: 50px;width: 50px;" src="<?php echo base_url(); ?>template/images/<?php echo getInfo("logo_perusahaan") ?>">
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><?php echo getInfo("nama_perusahaan") ?></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url('template/dist/img/avatar3.png'); ?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->session->userdata("pengguna")['nama']; ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo base_url('template/dist/img/avatar3.png'); ?>" class="img-circle" alt="User Image">
                  <p>
                    <?php echo $this->session->userdata("pengguna")['nama']; ?>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="<?php echo base_url("user/profile/$idAdmin") ?>" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo site_url("pengguna/logout") ?>" class="btn btn-default btn-flat">Logout</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
          <li class="header"></li>
          <!-- Optionally, you can add icons to the links -->

          <?php

          $setup[] = array("act" => "inf",
            "url" => "pengaturan",
            "authorize" => "informasiPerusahaan",
            "name" => 'Informasi Perusahaan');
          $setup[] = array("act" => "coa",
            "url" => "kodeakun",
            "authorize" => "kodeAkun",
            "name" => 'Kode Akun (COA)');
          $setup[] = array("act" => "daftarPelanggan",
            "url" => "daftarPelanggan",
            "authorize" => "daftarPelanggan",
            "name" => 'Daftar Pelanggan');
          $setup[] = array("act" => "ttd",
            "url" => "pengaturan/penandatanganan",
            "authorize" => "penandatanganan",
            "name" => 'Penandatanganan');
          $setup[] = array("act" => "konfigurasi",
            "url" => "konfigurasi",
            "authorize" => "konfigurasi",
            "name" => 'Konfigurasi');

          $transaksi[] = array("act" => "sawal",
            "url" => "journal/SaldoAwal",
            "authorize" => "saldoAwal",
            "name" => 'Saldo Awal');
          $transaksi[] = array("act" => "journal",
            "url" => "journal",
            "authorize" => "jurnal",
            "name" => 'Jurnal');


          $user[] = array("act" => "user",
            "url" => "user",
            "authorize" => "pengguna",
            "name" => 'Pengguna');
          $user[] = array("act" => "roles",
            "url" => "roles",
            "authorize" => "roles",
            "name" => 'Roles'); 
          $user[] = array("act" => "management",
            "url" => "roles/setting",
            "authorize" => "setRoles",
            "name" => 'Set Roles');

          $laporan[] = array("act" => "jurnalumum",
            "url" => "laporan/bukujurnal",
            "authorize" => "jurnalumum",
            "name" => 'Jurnal Umum');
          $laporan[] = array("act" => "bukubesar",
            "url" => "laporan/bukubesar",
            "authorize" => "bukubesar",
            "name" => 'Buku Besar'); 
          $laporan[] = array("act" => "labarugi",
            "url" => "laporan/labarugi",
            "authorize" => "labarugi",
            "name" => 'Laba Rugi');

          $laporan[] = array("act" => "ekuitas",
            "url" => "laporan/ekuitas",
            "authorize" => "ekuitas",
            "name" => 'Perubahan Ekuitas');
          $laporan[] = array("act" => "neraca",
            "url" => "laporan/neraca",
            "authorize" => "neraca",
            "name" => 'Neraca'); 
          $laporan[] = array("act" => "aruskas",
            "url" => "laporan/aruskas",
            "authorize" => "aruskas",
            "name" => 'Arus Kas');
          $laporan[] = array("act" => "lapPelanggan",
            "url" => "laporan/lapPelanggan",
            "authorize" => "lapPelanggan",
            "name" => 'Per Pelanggan');

          foreach ($setup as $key => $value) {
            $setupCount[] = $value['authorize'];
          }   

          foreach ($transaksi as $key => $value) {
            $transaksiCount[] = $value['authorize'];
          }

          foreach ($user as $key => $value) {
            $userCount[] = $value['authorize'];
          }

           foreach ($laporan as $key => $value) {
            $laporanCount[] = $value['authorize'];
          }

          ?>

          <li <?php echo @$act=='dash'?"class='active'":'' ?> ><a href="<?php echo base_url("dashboard") ?>"><i class="fa fa-tachometer"></i> <span>Beranda</span></a></li>

          <li style="display: <?php echo compareMenu($setupCount); ?>" <?php echo @$act=='inf' || @$act=='coa' || @$act=='ttd' || @$act=='konfigurasi' || @$act=='daftarPelanggan'  ?"class='treeview active'":'treeview' ?> >
            <a href="#"><i class="fa fa-cog" aria-hidden="true"></i> <span>Setup</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <?php foreach ($setup as $key => $value): ?>

                <?php if (menuAuth($value['authorize'])['condition']): ?>
                 <li <?php echo @$act==$value['act']? "class='active'":'' ?> ><a href="<?php echo base_url("$value[url]") ?>"><i class="fa fa-circle-o" aria-hidden="true"></i><?php echo  $value['name'] ?></a></li>
                 <?php else: ?>

                 <?php endif ?>

               <?php endforeach ?>
             </ul>
           </li>
           <li style="display: <?php echo compareMenu($transaksiCount); ?>"  <?php echo @$act=='journal' || @$act=='sawal' ?"class='treeview active'":'treeview' ?> >
            <a href="#"><i class="fa fa-money" aria-hidden="true"></i><span>Transaksi</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
             <?php foreach ($transaksi as $key => $value): ?>

              <?php if (menuAuth($value['authorize'])['condition']): ?>
               <li <?php echo @$act==$value['act']? "class='active'":'' ?> ><a href="<?php echo base_url("$value[url]") ?>"><i class="fa fa-circle-o" aria-hidden="true"></i><?php echo  $value['name'] ?></a></li>
               <?php else: ?>

               <?php endif ?>

             <?php endforeach ?>
             

           </ul>
         </li>
         <li style="display: <?php echo compareMenu($laporanCount); ?>" <?php echo @$act=='jurnalumum' || @$act=='bukubesar' || @$act=='labarugi' || @$act=='ekuitas' || @$act=='neraca' || @$act=='aruskas'  ?"class='treeview active'":'treeview' ?> >
          <a href="#">
            <i class="fa fa-file" aria-hidden="true">

            </i>
            <span>Laporan</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
           <?php foreach ($laporan as $key => $value): ?>

            <?php if (menuAuth($value['authorize'])['condition']): ?>
             <li <?php echo @$act==$value['act']? "class='active'":'' ?> ><a href="<?php echo base_url("$value[url]") ?>"><i class="fa fa-circle-o" aria-hidden="true"></i><?php echo  $value['name'] ?></a></li>
             <?php else: ?>

             <?php endif ?>

           <?php endforeach ?>


         </ul>
       </li>

       <li style="display: <?php echo compareMenu($userCount); ?>"  <?php echo @$act=='user' || @$act=='roles' || @$act=='management'  ?"class='treeview active'":'treeview' ?> >
        <a href="#">
          <i class="fa fa-user" aria-hidden="true">

          </i>
          <span>Manajemen Pengguna</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php foreach ($user as $key => $value): ?>

            <?php if (menuAuth($value['authorize'])['condition']): ?>
             <li <?php echo @$act==$value['act']? "class='active'":'' ?> ><a href="<?php echo base_url("$value[url]") ?>"><i class="fa fa-circle-o" aria-hidden="true"></i><?php echo  $value['name'] ?></a></li>
             <?php else: ?>

             <?php endif ?>

           <?php endforeach ?>
         </ul>
       </li>

       <li><a href="<?php echo site_url("pengguna/logout") ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
     </ul><!-- /.sidebar-menu -->
   </section>
   <!-- /.sidebar -->
 </aside>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->


  <!-- Main content -->
  <section class="content">

   <?php echo $this->session->userdata("pesan"); ?>

   <?php echo $konten; ?>
 </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->

  <!-- Default to the left -->
  <strong>  <?php echo getInfo("nama_perusahaan") ?></strong> All rights reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Create the tabs -->
  <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
  </ul>
  <!-- Tab panes -->

</aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
       <div class="control-sidebar-bg"></div>
     </div><!-- ./wrapper -->

     <!-- REQUIRED JS SCRIPTS -->
     <!-- Bootstrap 3.3.5 -->
     <script src="<?php echo base_url("template/bootstrap/js/bootstrap.min.js") ?>"></script>
     <!-- AdminLTE App -->
     <script src="<?php echo base_url("template/dist/js/app.min.js") ?>"></script>

     <script src="<?php echo base_url ("template/plugins/datatables/jquery.dataTables.min.js") ?>"></script>
     <script src="<?php echo base_url ("template/plugins/datatables/dataTables.bootstrap.min.js") ?>"></script>
     <script src="<?php echo base_url ("template/plugins/datepicker/bootstrap-datepicker.js") ?>"></script>
     <script src="<?php echo base_url ("template/plugins/accounting/accounting.min.js") ?>"></script>
     <script src="<?php echo base_url ("template/plugins/bootstrap-sweetalert/sweet-alert.min.js") ?>"></script>
     <script>
      $(function () {
        $("#tabelku").DataTable();

      });
    </script>
    <script src="<?php echo base_url("template/plugins/ckeditor/ckeditor.js") ?>"></script>
    <script src="<?php echo base_url("template/plugins/auto-numeric/autoNumeric.js") ?>" type="text/javascript"></script>
    <script>
      //CKEDITOR.replace("editorku");
    </script>

  </body>
  </html>
