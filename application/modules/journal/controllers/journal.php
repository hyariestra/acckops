<?php 



/**

* 

*/

class journal extends CI_Controller

{

	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))

		{
			
			redirect("pengguna/login");

		}

	}

	function index()
	{	
		authorize('jurnal');
		$data['act'] = 'journal';
		$data['cari'] =  $this->input->get('nomorCari');

		$data['idakun'] =  $this->input->get('IDAkun');

		$data['namaAKun'] =  $this->input->get('namaAKun');

		$data['start'] =  $this->input->get('start_date');
		$data['end'] =  $this->input->get('end_date');

		$data['convStart']  =  date("Y-m-d", strtotime($data['start']));
		$data['convEnd']  =  date("Y-m-d", strtotime($data['end']));

		$data['select-page'] = $this->input->get('select-page');
		$data['has'] = 'hide';


		$page = 30;

		if ($data['select-page']) {
			$page = $data['select-page'];
		}

		if ($data['cari']) {
			$query = $this->db->like("nomor", $data['cari']);
			$query = $this->db->or_like('uraian', $data['cari']);
			$data['has'] = 'show'; 
		}

		if ($data['idakun']) {
			$query = $this->db->like("id_akun", $data['idakun']);
			$data['has'] = 'show'; 
		}

		if ($data['start']) {
			$query = $this->db->where('tanggal >=', $data['convStart']);
			$query = $this->db->where('tanggal <=', $data['convEnd']);
			$data['has'] = 'show'; 
		} 
		

		$jumlah_data = $this->db->query("SELECT * FROM trx_ju order by nomor desc")->num_rows();

		$this->load->library('pagination');
		$config['base_url'] = base_url('journal/index');
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = $page;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		

		
		$query = $this->db->order_by("tanggal", "DESC");
		$query =  $this->db->join('trx_judet', 'trx_judet.id_ju = trx_ju.id_ju');
		$query =  $this->db->where('id_jenis_trans', '1');
		$query =  $this->db->group_by('trx_ju.id_ju');
		$query =  $this->db->get('trx_ju',$config['per_page'],$from)->result_array();	
		
		$data['jurnal'] = $query;


		foreach ($data['jurnal'] as $key => $value) {

			$data['detail'][] = $this->db->query("SELECT * FROM trx_judet LEFT JOIN mst_akun ON mst_akun.id_akun = trx_judet.id_akun WHERE id_ju = '".$value['id_ju']."' ")->result_array();
		}


		$data['judul'] = 'Daftar Transaksi';


		$this->themeadmin->tampilkan('listjournal',$data);
	}

	function SaldoAwal(){

		authorize('saldoAwal');
		$data['judul'] = 'Saldo Awal';
		$data['act'] = 'sawal';

		$this->themeadmin->tampilkan('saldoawal',$data);

	}

	function loadDataAkunSaldoAwal(){

		$this->load->model("M_journal", "ModelKodeAkun");

		echo $this->ModelKodeAkun->loadDataAkunSaldoAwal();

	}

	function loadDataAkunCariSaldoAwal(){

		$KataKunci = $this->input->post("data");

		$this->load->model("M_journal", "ModelKodeAkun");

		echo $this->ModelKodeAkun->loadDataAkunCariSaldoAwal($KataKunci);

	}

	function loadDataAkunCari(){

		$KataKunci = $this->input->post("data");

		$this->load->model("M_journal", "ModelKodeAkun");

		echo $this->ModelKodeAkun->loadDataAkunCari($KataKunci);

	}

	function SimpanSaldoAwal(){

		parse_str($this->input->post('data'), $data);

		$this->load->model('M_journal');

		$json = $this->M_journal->SimpanSaldoAwal($data);

		echo $json;

	}

	function tambah()
	{
		authorize('jurnal');
		$data['act'] = 'journal';
		$data['judul'] = 'Tambah Transaksi';

		$this->themeadmin->tampilkan('addjournal',$data);	
	}


	function edit()
	{
		authorize('jurnal');
		$data['act'] = 'journal';
		$IDJU      = $this->input->get('IDJU');

		$data['jurnal'] = $this->db->query("SELECT * FROM trx_ju WHERE id_ju = '".$IDJU."' ")->result_array();

		$data['judul'] = 'Edit Transaksi';

		$this->themeadmin->tampilkan('editjournal',$data);	
	}

	function loadDataAkun(){

		$this->load->model("M_journal", "ModelKodeAkun");

		echo $this->ModelKodeAkun->loadDataAkun();

	}

	function getAutoNum(){

		$tipe = $this->input->post("tipe");

		$hasil = GetNextNo(GetAutoNum($tipe));

		echo json_encode($hasil);

	}

	function SimpanData(){

		$Nomor 		= $this->input->post("Nomor");
		$Tanggal 	= formatDateDB($this->input->post("Tanggal"));
		$Uraian 	= $this->input->post("Uraian");
		$IDPelanggan 	= $this->input->post("IDPelanggan");

		parse_str($this->input->post('FormSerialize'), $data);

		$this->load->model("M_journal", "ModelKodeAkun");

		$hasil = $this->ModelKodeAkun->SimpanJU($Nomor, $Tanggal, $Uraian, $IDPelanggan, $data);

		echo $hasil;

	}

	function EditData(){

		$Nomor 		= $this->input->post("Nomor");
		$Tanggal 	= formatDateDB($this->input->post("Tanggal"));
		$Uraian 	= $this->input->post("Uraian");
		$IDJU    	= $this->input->post("IDJU");
		$IDPelanggan    	= $this->input->post("IDPelanggan");

		parse_str($this->input->post('FormSerialize'), $data);

		$this->load->model("M_journal", "ModelKodeAkun");

		$hasil = $this->ModelKodeAkun->EditJU($Nomor, $Tanggal, $Uraian, $IDPelanggan, $data, $IDJU);

		echo $hasil;

	}

	function HapusData(){

		$IDJU 		= $this->input->post("IDJurnal");


		$this->load->model("M_journal", "ModelKodeAkun");

		$hasil = $this->ModelKodeAkun->HapusJU($IDJU);

		$isipesan="<br><div class='alert alert-success'>Data Berhasil Dihapus!</div>";
		$this->session->set_flashdata("pesan",$isipesan);
		redirect("journal/index");

	}


}





?>

