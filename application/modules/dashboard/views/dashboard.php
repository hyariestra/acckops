<!-- <?php echo $judul ?> 
<?php echo $this->session->userdata("pengguna")['nama']; ?>
-->

<script src="<?php echo base_url("template/plugins/highchartjs/highcharts.js") ?>
"></script>
<script src="<?php echo base_url("template/plugins/highchartjs/export-data.js") ?>"></script>
<script src="<?php echo base_url("template/plugins/highchartjs/exporting.js") ?>"></script>




<div class="row">

	<div class="col-md-12">


		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title"></h3>

				<div class="pull-right row col-sm-2">
					<div class="input-group date">  
						<form id="formId" action="<?php echo base_url("dashboard") ?>" method="GET" >
							<input style="background-color: white" readonly="" value="<?php echo $yearNow; ?>" type="text" class="form-control datepicker" name="year" >
						</form>                        
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>

					</div>       
				</div>

			</div>

			
			<div id="chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

			</div>

		</div>
	</div>
</div>


<script>

	$('document').ready(function()
	{

		$('.datepicker').datepicker({
			autoclose: true,
			format: "yyyy",
			viewMode: "years", 
			minViewMode: "years"
		}).on('changeDate',function(ev){
				$('#formId').submit();
		});		

	}); 


	Highcharts.chart('chart', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Perbandingan Pemasukan dan Pengeluaran Tahun <?php echo  $yearNow ?>'
    },
    xAxis: {
      	categories: <?php echo json_encode($bulan) ?>
    },
    credits: {
        enabled: false
    },
    series: [{
      name: 'Pemasukan',
    	data: <?php echo json_encode($ArrPemasukan)?>

    },  {
       name: 'Pengeluaran',
    	data: <?php echo json_encode($ArrPengeluaran)?>
    }]
});



	
</script>

