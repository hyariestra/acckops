<?php 



/**

* 

*/

class M_pelanggan extends CI_model

{


  function input_data($data){


    $data['kode'];

    $this->db->where("kode",$data['kode']);

    $cek = $this->db->get("mst_pelanggan");

    $hitung = $cek->num_rows(); 


    if ($hitung > 0) {
      return "gagal";
    }else{
      $this->db->insert('mst_pelanggan',$data);
      return "sukses";
    }

  }

  function update_data($id,$data){


    $data['kode'];

    $this->db->where("kode",$data['kode']);
    $this->db->where_not_in("id",$id);

    $cek = $this->db->get("mst_pelanggan");

    $hitung = $cek->num_rows(); 


    if ($hitung > 0) {
      return "gagal";
    }else{

      $datapeng=elements(array('nama_pelanggan','kode','alamat_pelanggan','telp1','telp2','email'),

        $data,"-");

      $this->db->where('id',$id);
      $this->db->update('mst_pelanggan',$datapeng);

      return "sukses";
    }

  }


  public function getAllPelanggan()
  {
    $query = $this->db->get('mst_pelanggan');

    return $query;

  }

  public function hapus_data($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('mst_pelanggan');
  }

  public function getEditPelanggan($id)
  {
    $query = $this->db->get_where('mst_pelanggan', array('id' => $id));

    return $query;
  }


}



?>