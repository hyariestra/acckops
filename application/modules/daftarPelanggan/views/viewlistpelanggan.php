<style type="text/css" media="screen">
	#jurnalTable td, #jurnalTable th {
		border: 1px solid #ddd;
		padding: 8px;
	}
</style>
<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $judul ?></h3>
				<div class="box-tools pull-right">
					<span class="btn btn-sm search-btn"><i class="fa fa-search"></i></span>              
				</div>
			</div>

			<div class="box-tools pull-right">

			</div>

			<div class="box-body">
				<div class="custom-table-wrapper">
					
					<div class="table-filter table-filter__wrapper">
						<div class="row">
							<div class="col-xs-8">
								<div class="table-filter__left">
									<a href="<?php echo site_url('daftarPelanggan/tambah') ?>" class="btn btn-filter btn-danger "><i class="fa fa-plus"></i> Tambah Pelanggan Baru</a>

								</div>
							</div>
							
						</div>
					</div>
					<div style="margin-top: 20px">
						<table id="tabelku" class="main-table table table-bordered table-striped">
							<thead>
								<tr>
									<th class="no-sort">Nama Pelanggan</th>
									<th class="no-sort">Kode</th>
									<th class="no-sort">Alamat Pelanggan</th>
									<th class="no-sort">Nomor Telepon 1</th>
									<th class="no-sort">Nomor Telepon 2</th>
									<th class="no-sort">Email</th>
									<th width="100" class="no-sort">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php

								foreach ($query->result_array() as $key =>$value) {

									?>
									<tr>
										<td class="no-sort"><?php echo $value['nama_pelanggan'] ?></td>
										<td class="no-sort"><?php echo $value['kode'] ?></td>
										<td class="no-sort"><?php echo $value['alamat_pelanggan'] ?></td>
										<td class="no-sort"><?php echo $value['telp1'] ?></td>
										<td class="no-sort"><?php echo $value['telp2'] ?></td>
										<td class="no-sort"><?php echo $value['email'] ?></td>
										<td>
											<form action="<?php echo base_url('daftarPelanggan/HapusData'); ?>" method="post" accept-charset="utf-8">
												<input type="hidden" id="IDJurnal" name="idPelanggan" value="<?php echo $value['id'] ?>">
												<a href="<?php echo base_url("daftarPelanggan/edit/$value[id]"); ?>" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
												<button type="submit" class="button_delete btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
											</form>

										</td>
									</tr>
								<?php } ?>
							</tbody>
							
						</table>  
					</div>
					

			</div>
		</div>

	</div>
</div>	
</div>



<script>
	
		$('.button_delete').on('click',function(e){
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
			title: "Apakah Anda Yakin",
			text: "Data yang telah dihapus tidak dapat dikembalikan",
			type: "error",
			showCancelButton: true,
			cancelButtonClass: 'btn-default btn-md waves-effect',
			confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
			confirmButtonText: 'Delete!'
		}, 
		function(willDelete) {
			if (willDelete) {
				form.submit();

			}     
		});
			});
</script>