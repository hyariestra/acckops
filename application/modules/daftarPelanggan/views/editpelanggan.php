<style type="text/css" media="screen">
	.control-label{
		text-align: left !important;
	}
</style>
<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $judul ?></h3>

			</div>
			<form action="<?php echo base_url("daftarPelanggan/update/$hasil->id") ?>" method="post" accept-charset="utf-8">
				<div class="box-body">
					<div class="form-horizontal text-left">
						<div class="box-body">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">No Kode #</label>
								<div class="col-sm-10">
									<input required="" value="<?php echo $hasil->kode; ?>"  class="form-control" placeholder="masukan no kode.." name="kode" type="text">
									<i style="font-size: 13px;" >*Nomor Kode bersifat Unik, tidak dapat menyimpan nomor yang sama dan wajiib di isi</i>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Nama Pelanggan</label>
								<div class="col-sm-10">
									<input required="" value="<?php echo $hasil->nama_pelanggan;  ?>" class="form-control" placeholder="masukan nama pelanggan.." name="nama_pelanggan" type="text">
									<i style="font-size: 13px;" >*Nama pelanggan wajib diisi</i>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">No Telepon 1</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon" id="sizing-addon2"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
										<input value="<?php echo $hasil->telp1 ?>" type="text" class="form-control" name="telp1" placeholder="masukan nomor telepon..." aria-describedby="sizing-addon2">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">No Telepon 2</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon" id="sizing-addon2"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
										<input value="<?php echo $hasil->telp2 ?>" type="text" class="form-control" name="telp2" placeholder="masukan nomor telepon..." aria-describedby="sizing-addon2">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon" id="sizing-addon2"><i class="fa fa-envelope" aria-hidden="true"></i></span>
										<input value="<?php echo $hasil->email ?>" type="email" class="form-control" name="email" placeholder="masukan email..." aria-describedby="sizing-addon2">
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-top: 20px;">
								<label for="" class="col-sm-2 control-label">Alamat Pelanggan</label>
								<div class="col-sm-10">
									<textarea  class="form-control" name="alamat_pelanggan" id="Uraian" rows="3" placeholder="masukan alamat..."><?php echo $hasil->alamat_pelanggan ?></textarea>
								</div>
							</div>


						</div>

						<div class="box-footer text-right">
							<a href="<?php echo site_url('daftarPelanggan') ?>" class="btn btn-default">Batal</a>
							<button type="submit" id="simpan" class="btn btn-primary">Update</button>
						</div>

					</div>
					<!-- /.col -->
				</div>
			</form>
		</div>

	</div>
</div>	



