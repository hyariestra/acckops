<?php 



/**

* 

*/

class daftarPelanggan extends CI_Controller

{

	function __construct()

	{

		parent::__construct();

		$this->load->model('M_pelanggan');

		if (!$this->session->userdata("pengguna"))

		{
			
			redirect("pengguna/login");

		}

	}

	function index()
	{	
		authorize('daftarPelanggan');

		$data['judul'] = 'Daftar Pelanggan';

		$data['act'] = 'daftarPelanggan';

		$data['query'] = $this->M_pelanggan->getAllPelanggan();


		$this->themeadmin->tampilkan('viewlistpelanggan.php',$data);
	}




	function tambah()
	{
		authorize('daftarPelanggan');
		$data['act'] = 'daftarPelanggan';
		$data['judul'] = 'Tambah Pelanggan';


		$this->themeadmin->tampilkan('addpelanggan',$data);	
	}


	function edit($id)
	{
		authorize('daftarPelanggan');

		$data['act'] = 'daftarPelanggan';

		$hasil = $this->M_pelanggan->getEditPelanggan($id);


		$data['hasil'] = $hasil->row();
		
		$data['judul'] = 'Edit Pelanggan';

		$this->themeadmin->tampilkan('editpelanggan',$data);	
	}



	function simpan(){


		
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama_pelanggan', 'nama_pelanggan', 'required');
		$this->form_validation->set_rules('kode', 'kode', 'required',
			array('required' => 'You must provide a %s.')
		);

		if ($this->form_validation->run() == FALSE)
		{
			$isipesan="<br><div class='alert alert-danger'>Data Masih Ada yang kosong ,cek lagi inputan anda!</div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect("daftarPelanggan/tambah");
		}
		else
		{
			$data = array(
				'nama_pelanggan' => $this->input->post('nama_pelanggan'),
				'kode' => $this->input->post('kode'),
				'alamat_pelanggan' =>$this->input->post('alamat_pelanggan'),
				'telp1' => $this->input->post('telp1'),
				'telp2' => $this->input->post('telp2'),
				'email' => $this->input->post('email')
			);

			$hasil = $this->M_pelanggan->input_data($data);

			if ($hasil=='sukses') {
				$isipesan="<br><div class='alert alert-success'>Data Berhasil Disimpan!</div>";
			}else{
				$isipesan="<br><div class='alert alert-danger'>Data Gagal Disimpan Nomor Kode Sudah Pernah Digunakan!</div>";	

			}
			$this->session->set_flashdata("pesan",$isipesan);
			redirect("daftarPelanggan/tambah");
		}

	}

	function update($id){

		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama_pelanggan', 'nama_pelanggan', 'required');
		$this->form_validation->set_rules('kode', 'kode', 'required',
			array('required' => 'You must provide a %s.')
		);

		if ($this->form_validation->run() == FALSE)
		{
			$isipesan="<br><div class='alert alert-danger'>Data Masih Ada yang kosong ,cek lagi inputan anda!</div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect("daftarPelanggan/edit/$id");
		}
		else
		{
			$data = array(
				'nama_pelanggan' => $this->input->post('nama_pelanggan'),
				'kode' => $this->input->post('kode'),
				'alamat_pelanggan' =>$this->input->post('alamat_pelanggan'),
				'telp1' => $this->input->post('telp1'),
				'telp2' => $this->input->post('telp2'),
				'email' => $this->input->post('email')
			);

			$hasil = $this->M_pelanggan->update_data($id,$data);

			if ($hasil=='sukses') {
				$isipesan="<br><div class='alert alert-success'>Data Berhasil Disimpan!</div>";
			}else{
				$isipesan="<br><div class='alert alert-danger'>Data Gagal Disimpan Nomor Kode Sudah Pernah Digunakan!</div>";	

			}
			$this->session->set_flashdata("pesan",$isipesan);
			redirect("daftarPelanggan/edit/$id");
		}




	}

	function HapusData(){

		$idp = $this->input->post("idPelanggan");


		$this->M_pelanggan->hapus_data($idp);

		$isipesan="<br><div class='alert alert-success'>Data Berhasil Dihapus!</div>";
		$this->session->set_flashdata("pesan",$isipesan);
		redirect("daftarPelanggan");

	}


}





?>

