<?php 



/**

* 

*/

class laporan extends CI_Controller

{


  	function __construct()

	{

		parent::__construct();


		if (!$this->session->userdata("pengguna"))

		{
			
			redirect("Pengguna/login");

		}

	}


	function index()
	{	
		
		$data['judul'] = 'Pilih Laporan';


		$this->themeadmin->tampilkan('pilihlaporan',$data);
	}

	function bukujurnal(){

		authorize('jurnalumum');
		$data['act'] = 'jurnalumum';

		$data['judul'] = 'Pilih Periode Laporan Buku Jurnal';

		$data['html']  = 'showBukuJurnal';


		$this->themeadmin->tampilkan('pilihlaporan',$data);

	}

	function bukubesar(){

		authorize('bukubesar');
		$data['act'] = 'bukubesar';

		$data['judul'] = 'Pilih Periode Laporan Buku Besar';

		$data['html']  = 'showBukuBesar';


		$this->themeadmin->tampilkan('pilihlaporan_bukubesar',$data);

	}

	function labarugi(){

		authorize('labarugi');
		$data['act'] = 'labarugi';

		$data['judul'] = 'Pilih Periode Laporan Laba Rugi';

		$data['html']  = 'showLabaRugi';


		$this->themeadmin->tampilkan('pilihlaporan',$data);

	}

	function ekuitas(){

		authorize('ekuitas');
		$data['act'] = 'ekuitas';
		$data['judul'] = 'Pilih Periode Laporan Perubahan Ekuitas';

		$data['html']  = 'showEkuitas';


		$this->themeadmin->tampilkan('pilihlaporan',$data);

	}

	function neraca(){

		authorize('neraca');
		$data['act'] = 'neraca';
		$data['judul'] = 'Pilih Periode Laporan Neraca';

		$data['html']  = 'showNeraca';


		$this->themeadmin->tampilkan('pilihlaporan',$data);

	}

	function aruskas(){

		authorize('aruskas');
		$data['act'] = 'aruskas';
		$data['judul'] = 'Pilih Periode Laporan Arus Kas';

		$data['html']  = 'showArusKas';


		$this->themeadmin->tampilkan('pilihlaporan',$data);

	}

	function lapPelanggan(){

		authorize('lapPelanggan');
		$data['act'] = 'lapPelanggan';
		$data['judul'] = 'Pilih Periode Laporan Per Pelanggan';

		$data['html']  = 'showLapPelanggan';


		$this->themeadmin->tampilkan('pilihlaporan_pelanggan',$data);

	}

	public function showBukuJurnal(){

		$content = $this->load->view('buku_jurnal', '', true);

		echo $content;

	}

	public function showLapPelanggan(){

		$IDPelanggan   =  $this->uri->segment(3);

		$periode       =  $this->uri->segment(4); 

		$nilai         =  $this->uri->segment(5);

		$tahunPeriode        =  $this->uri->segment(7);


		$startMonth = 1;

		$strPeriodeHeader = '';

		$strPeriode = '';

		$strPeriodePembanding = '';

		$tahun = $tahunPeriode;

		if ($periode == 'Bulan')
		{
			$startDate = $tahun.'-'.$nilai.'-'.'01';
			$endDate   = $tahun.'-'.$nilai.'-'.'31';
			$tanggal   = GetLastDateInMonth($nilai);
			$strPeriodeHeader     = $tanggal. " ".GetMonthName($nilai)." ".$tahun;
			$strPeriode           = GetMonthName($nilai)." ".$tahun;
			$bulanPembanding     = ($nilai-1 ) > 1 ?  $nilai-1 : 1;
			$strPeriodePembanding = GetMonthName($bulanPembanding)." ".$tahun;
			$bulanberikut         = $nilai+1;
			$strPeriodeBerikut = GetMonthName($bulanberikut)." ".$tahun;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$endDateCompare   = $tahun.'-'.$bulanPembanding.'-'.'31';
		}

		if ($periode == 'Triwulan')
		{
			$startMonth = ($nilai == 1) ? 1 : 0; 
			$endMonth   = ($nilai == 1) ? 3 : 0;
			$startMonth = ($nilai == 2) ? 4 : $startMonth;
			$endMonth   = ($nilai == 2) ? 6 : $endMonth;
			$startMonth = ($nilai == 3) ? 7 : $startMonth;
			$endMonth   = ($nilai == 3) ? 9 : $endMonth;
			$startMonth = ($nilai == 4) ? 10 : $startMonth;
			$endMonth   = ($nilai == 4) ? 12 : $endMonth;                 
			$startDate  = $tahun.'-'.$startMonth.'-'.'01';
			$endDate    = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal    = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Triwulan '.$nilai.' Tahun 2019';
			$strPeriode            = 'Triwulan '.$nilai;
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 3;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Triwulan '.$nilaiPembanding; 
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 1;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanAkhirPembanding = ($endMonth-3) > 1 ?  $endMonth-3 : 3;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Semester')
		{
			$startMonth = ($nilai == 1) ? 1 : 1;
			$endMonth   = ($nilai == 1) ? 6 : 6;
			$startMonth = ($nilai == 2) ? 7 : $startMonth;
			$endMonth   = ($nilai == 2) ? 12 : $endMonth;      
			$startDate = $tahun.'-'.$startMonth.'-'.'01';
			$endDate   = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal   = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Semester '.$nilai.' Tahun 2019';
			$strPeriode            = 'Semester '.$nilai;
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 1;
			$startDateCompare     = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 6;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Semester '.$nilaiPembanding; 
			$bulanAkhirPembanding = ($endMonth-6) > 1 ?  $endMonth-6 : 6;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Tahun')
		{
			$startDate = $tahun.'-01-01';
			$endDate   = $tahun.'-12-31';
			$tanggal = GetLastDateInMonth(12, $tahun);
			$strPeriodeHeader       = 'Tahun '.$tahun;
            $strPeriode             = $strPeriodeHeader;//GetMonthName(12)." ".$nilai;
            $tahunSebelumnya        = $tahun-1;
            $startDateCompare       = $tahunSebelumnya.'-01-01';
            $nilaiPembanding        = $tahun - 1;
            $strPeriodePembanding   = 'Tahun '.$nilaiPembanding;
            $endDateCompare         = $tahunSebelumnya.'-12-31';

        } 

        $this->load->model("M_laporan", "ModelLaporan");

        $data["isitabel"] 	= $this->ModelLaporan->getLaporanPelanggan($IDPelanggan, $startDate, $endDate, $startDateCompare, $endDateCompare);

        $data["strPeriode"] = $strPeriode;

        $data["pelanggan"] = $this->db->query("SELECT nama_pelanggan, kode FROM mst_pelanggan WHERE id = '".$IDPelanggan."'");

        $content = $this->load->view('lap_pelanggan', $data, true);

        echo $content;

	}

	public function showBukuBesar(){

		$periode       =  $this->uri->segment(3); 

		$nilai         =  $this->uri->segment(4);

		$IDAkun        =  $this->uri->segment(6);

		$tahunPeriode        =  $this->uri->segment(7);


		$startMonth = 1;

		$strPeriodeHeader = '';

		$strPeriode = '';

		$strPeriodePembanding = '';

		$tahun = $tahunPeriode;

		if ($periode == 'Bulan')
		{
			$startDate = $tahun.'-'.$nilai.'-'.'01';
			$endDate   = $tahun.'-'.$nilai.'-'.'31';
			$tanggal   = GetLastDateInMonth($nilai);
			$strPeriodeHeader     = $tanggal. " ".GetMonthName($nilai)." ".$tahun;
			$strPeriode           = GetMonthName($nilai)." ".$tahun;
			$bulanPembanding     = ($nilai-1 ) > 1 ?  $nilai-1 : 1;
			$strPeriodePembanding = GetMonthName($bulanPembanding)." ".$tahun;
			$bulanberikut         = $nilai+1;
			$strPeriodeBerikut = GetMonthName($bulanberikut)." ".$tahun;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$endDateCompare   = $tahun.'-'.$bulanPembanding.'-'.'31';
		}

		if ($periode == 'Triwulan')
		{
			$startMonth = ($nilai == 1) ? 1 : 0; 
			$endMonth   = ($nilai == 1) ? 3 : 0;
			$startMonth = ($nilai == 2) ? 4 : $startMonth;
			$endMonth   = ($nilai == 2) ? 6 : $endMonth;
			$startMonth = ($nilai == 3) ? 7 : $startMonth;
			$endMonth   = ($nilai == 3) ? 9 : $endMonth;
			$startMonth = ($nilai == 4) ? 10 : $startMonth;
			$endMonth   = ($nilai == 4) ? 12 : $endMonth;                 
			$startDate  = $tahun.'-'.$startMonth.'-'.'01';
			$endDate    = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal    = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Triwulan '.$nilai.' Tahun 2019';
			$strPeriode            = 'Triwulan '.$nilai;
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 3;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Triwulan '.$nilaiPembanding; 
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 1;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanAkhirPembanding = ($endMonth-3) > 1 ?  $endMonth-3 : 3;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Semester')
		{
			$startMonth = ($nilai == 1) ? 1 : 1;
			$endMonth   = ($nilai == 1) ? 6 : 6;
			$startMonth = ($nilai == 2) ? 7 : $startMonth;
			$endMonth   = ($nilai == 2) ? 12 : $endMonth;      
			$startDate = $tahun.'-'.$startMonth.'-'.'01';
			$endDate   = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal   = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Semester '.$nilai.' Tahun 2019';
			$strPeriode            = 'Semester '.$nilai;
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 1;
			$startDateCompare     = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 6;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Semester '.$nilaiPembanding; 
			$bulanAkhirPembanding = ($endMonth-6) > 1 ?  $endMonth-6 : 6;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Tahun')
		{
			$startDate = $tahun.'-01-01';
			$endDate   = $tahun.'-12-31';
			$tanggal = GetLastDateInMonth(12, $tahun);
			$strPeriodeHeader       = 'Tahun '.$tahun;
		                $strPeriode             = $strPeriodeHeader;//GetMonthName(12)." ".$nilai;
		                $tahunSebelumnya        = $tahun-1;
		                $startDateCompare       = $tahunSebelumnya.'-01-01';
		                $nilaiPembanding        = $tahun - 1;
		                $strPeriodePembanding   = 'Tahun '.$nilaiPembanding;
		                $endDateCompare         = $tahunSebelumnya.'-12-31';

		            } 

		            $this->load->model("M_laporan", "ModelLaporan");

		            $data["saldoawal"]  = $this->ModelLaporan->getSaldoAwalBukuBesar($IDAkun);

		            $data["isitabel"] 	= $this->ModelLaporan->getLaporanBukuBesar($IDAkun, $startDate, $endDate, $startDateCompare, $endDateCompare);

		            $data["strPeriode"] = $strPeriode;

		            $content = $this->load->view('buku_besar', $data, true);

		            echo $content;

		        }

		        public function showEkuitas(){

		        	$periode       =  $this->uri->segment(3); 

		        	$nilai         =  $this->uri->segment(4);

		        	$tahunPeriode        =  $this->uri->segment(6);


		        	$startMonth = 1;

		        	$strPeriodeHeader = '';

		        	$strPeriode = '';

		        	$strPeriodePembanding = '';

		        	$tahun = $tahunPeriode;

		        	if ($periode == 'Bulan')
		        	{
		        		$startDate = $tahun.'-'.$nilai.'-'.'01';
		        		$endDate   = $tahun.'-'.$nilai.'-'.'31';
		        		$tanggal   = GetLastDateInMonth($nilai);
		        		$strPeriodeHeader     = $tanggal. " ".GetMonthName($nilai)." ".$tahun;
		        		$strPeriode           = GetMonthName($nilai)." ".$tahun;
		        		$bulanPembanding     = ($nilai-1 ) > 1 ?  $nilai-1 : 1;
		        		$strPeriodePembanding = GetMonthName($bulanPembanding)." ".$tahun;
		        		$bulanberikut         = $nilai+1;
		        		$strPeriodeBerikut = GetMonthName($bulanberikut)." ".$tahun;
		        		$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
		        		$endDateCompare   = $tahun.'-'.$bulanPembanding.'-'.'31';
		        	}

		        	if ($periode == 'Triwulan')
		        	{
		        		$startMonth = ($nilai == 1) ? 1 : 0; 
		        		$endMonth   = ($nilai == 1) ? 3 : 0;
		        		$startMonth = ($nilai == 2) ? 4 : $startMonth;
		        		$endMonth   = ($nilai == 2) ? 6 : $endMonth;
		        		$startMonth = ($nilai == 3) ? 7 : $startMonth;
		        		$endMonth   = ($nilai == 3) ? 9 : $endMonth;
		        		$startMonth = ($nilai == 4) ? 10 : $startMonth;
		        		$endMonth   = ($nilai == 4) ? 12 : $endMonth;                 
		        		$startDate  = $tahun.'-'.$startMonth.'-'.'01';
		        		$endDate    = $tahun.'-'.$endMonth.'-'.'31';
		        		$tanggal    = GetLastDateInMonth($endMonth);
		        		$strPeriodeHeader     = 'Triwulan '.$nilai.' Tahun 2019';
		        		$strPeriode            = 'Triwulan '.$nilai;
		        		$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 3;
		        		$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
		        		$strPeriodePembanding = 'Triwulan '.$nilaiPembanding; 
		        		$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 1;
		        		$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
		        		$bulanAkhirPembanding = ($endMonth-3) > 1 ?  $endMonth-3 : 3;
		        		$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		        	}

		        	if ($periode == 'Semester')
		        	{
		        		$startMonth = ($nilai == 1) ? 1 : 1;
		        		$endMonth   = ($nilai == 1) ? 6 : 6;
		        		$startMonth = ($nilai == 2) ? 7 : $startMonth;
		        		$endMonth   = ($nilai == 2) ? 12 : $endMonth;      
		        		$startDate = $tahun.'-'.$startMonth.'-'.'01';
		        		$endDate   = $tahun.'-'.$endMonth.'-'.'31';
		        		$tanggal   = GetLastDateInMonth($endMonth);
		        		$strPeriodeHeader     = 'Semester '.$nilai.' Tahun 2019';
		        		$strPeriode            = 'Semester '.$nilai;
		        		$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 1;
		        		$startDateCompare     = $tahun.'-'.$bulanPembanding.'-'.'01';
		        		$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 6;
		        		$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
		        		$strPeriodePembanding = 'Semester '.$nilaiPembanding; 
		        		$bulanAkhirPembanding = ($endMonth-6) > 1 ?  $endMonth-6 : 6;
		        		$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		        	}

		        	if ($periode == 'Tahun')
		        	{
		        		$startDate = $tahun.'-01-01';
		        		$endDate   = $tahun.'-12-31';
		        		$tanggal = GetLastDateInMonth(12, $tahun);
		        		$strPeriodeHeader       = 'Tahun '.$tahun;
		                $strPeriode             = $strPeriodeHeader;//GetMonthName(12)." ".$nilai;
		                $tahunSebelumnya        = $tahun-1;
		                $startDateCompare       = $tahunSebelumnya.'-01-01';
		                $nilaiPembanding        = $tahun - 1;
		                $strPeriodePembanding   = 'Tahun '.$nilaiPembanding;
		                $endDateCompare         = $tahunSebelumnya.'-12-31';

		            } 

		            $this->load->model("M_laporan", "ModelLaporan");

		$data["saldoawal"]  = '';//$this->ModelLaporan->getSaldoAwalBukuBesar();

		$data["isitabel"] 	= $this->ModelLaporan->getLaporanEkuitas($startDate, $endDate, $startDateCompare, $endDateCompare, $strPeriodeHeader, $strPeriodePembanding);

		$data["strPeriode"] = $strPeriode;

		$data["strPeriodeLaporan"] = $strPeriodeHeader;	

		$content = $this->load->view('perubahan_ekuitas', $data, true);

		echo $content;

	}

	public function showLabaRugi(){

		$periode       =  $this->uri->segment(3); 

		$nilai         =  $this->uri->segment(4);

		$tahunPeriode        =  $this->uri->segment(6);


		$startMonth = 1;

		$strPeriodeHeader = '';

		$strPeriode = '';

		$strPeriodePembanding = '';

		$tahun = $tahunPeriode;

		if ($periode == 'Bulan')
		{
			$startDate = $tahun.'-'.$nilai.'-'.'01';
			$endDate   = $tahun.'-'.$nilai.'-'.'31';
			$tanggal   = GetLastDateInMonth($nilai);
			$strPeriodeHeader     = $tanggal. " ".GetMonthName($nilai)." ".$tahun;
			$strPeriode           = GetMonthName($nilai)." ".$tahun;
			$bulanPembanding     = ($nilai-1 ) > 1 ?  $nilai-1 : 1;
			$strPeriodePembanding = GetMonthName($bulanPembanding)." ".$tahun;
			$bulanberikut         = $nilai+1;
			$strPeriodeBerikut = GetMonthName($bulanberikut)." ".$tahun;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$endDateCompare   = $tahun.'-'.$bulanPembanding.'-'.'31';
		}

		if ($periode == 'Triwulan')
		{
			$startMonth = ($nilai == 1) ? 1 : 0; 
			$endMonth   = ($nilai == 1) ? 3 : 0;
			$startMonth = ($nilai == 2) ? 4 : $startMonth;
			$endMonth   = ($nilai == 2) ? 6 : $endMonth;
			$startMonth = ($nilai == 3) ? 7 : $startMonth;
			$endMonth   = ($nilai == 3) ? 9 : $endMonth;
			$startMonth = ($nilai == 4) ? 10 : $startMonth;
			$endMonth   = ($nilai == 4) ? 12 : $endMonth;                 
			$startDate  = $tahun.'-'.$startMonth.'-'.'01';
			$endDate    = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal    = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Triwulan '.$nilai.' Tahun 2019';
			$strPeriode            = 'Triwulan '.$nilai;
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 3;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Triwulan '.$nilaiPembanding; 
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 1;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanAkhirPembanding = ($endMonth-3) > 1 ?  $endMonth-3 : 3;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Semester')
		{
			$startMonth = ($nilai == 1) ? 1 : 1;
			$endMonth   = ($nilai == 1) ? 6 : 6;
			$startMonth = ($nilai == 2) ? 7 : $startMonth;
			$endMonth   = ($nilai == 2) ? 12 : $endMonth;      
			$startDate = $tahun.'-'.$startMonth.'-'.'01';
			$endDate   = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal   = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Semester '.$nilai.' Tahun 2019';
			$strPeriode            = 'Semester '.$nilai;
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 1;
			$startDateCompare     = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 6;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Semester '.$nilaiPembanding; 
			$bulanAkhirPembanding = ($endMonth-6) > 1 ?  $endMonth-6 : 6;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Tahun')
		{
			$startDate = $tahun.'-01-01';
			$endDate   = $tahun.'-12-31';
			$tanggal = GetLastDateInMonth(12, $tahun);
			$strPeriodeHeader       = 'Tahun '.$tahun;
		                $strPeriode             = $strPeriodeHeader;//GetMonthName(12)." ".$nilai;
		                $tahunSebelumnya        = $tahun-1;
		                $startDateCompare       = $tahunSebelumnya.'-01-01';
		                $nilaiPembanding        = $tahun - 1;
		                $strPeriodePembanding   = 'Tahun '.$nilaiPembanding;
		                $endDateCompare         = $tahunSebelumnya.'-12-31';

		            } 

		            $this->load->model("M_laporan", "ModelLaporan");

		$data["saldoawal"]  = '';//$this->ModelLaporan->getSaldoAwalBukuBesar();

		$data["isitabel"] 	= $this->ModelLaporan->getLaporanLabaRugi($startDate, $endDate, $startDateCompare, $endDateCompare);

		$data["strPeriode"] = $strPeriode;

		$data["strPeriodeLaporan"] = $strPeriodeHeader;

		$content = $this->load->view('laba_rugi', $data, true);

		echo $content;

	}

	public function showNeraca(){

		$periode       =  $this->uri->segment(3); 

		$nilai         =  $this->uri->segment(4);

		$tahunPeriode        =  $this->uri->segment(6);


		$startMonth = 1;

		$strPeriodeHeader = '';

		$strPeriode = '';

		$strPeriodePembanding = '';

		$tahun = $tahunPeriode;

		if ($periode == 'Bulan')
		{
			$startDate = $tahun.'-'.$nilai.'-'.'01';
			$endDate   = $tahun.'-'.$nilai.'-'.'31';
			$tanggal   = GetLastDateInMonth($nilai);
			$strPeriodeHeader     = $tanggal. " ".GetMonthName($nilai)." ".$tahun;
			$strPeriode           = GetMonthName($nilai)." ".$tahun;
			$bulanPembanding     = ($nilai-1 ) > 1 ?  $nilai-1 : 1;
			$strPeriodePembanding = GetMonthName($bulanPembanding)." ".$tahun;
			$bulanberikut         = $nilai+1;
			$strPeriodeBerikut = GetMonthName($bulanberikut)." ".$tahun;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$endDateCompare   = $tahun.'-'.$bulanPembanding.'-'.'31';
		}

		if ($periode == 'Triwulan')
		{
			$startMonth = ($nilai == 1) ? 1 : 0; 
			$endMonth   = ($nilai == 1) ? 3 : 0;
			$startMonth = ($nilai == 2) ? 4 : $startMonth;
			$endMonth   = ($nilai == 2) ? 6 : $endMonth;
			$startMonth = ($nilai == 3) ? 7 : $startMonth;
			$endMonth   = ($nilai == 3) ? 9 : $endMonth;
			$startMonth = ($nilai == 4) ? 10 : $startMonth;
			$endMonth   = ($nilai == 4) ? 12 : $endMonth;                 
			$startDate  = $tahun.'-'.$startMonth.'-'.'01';
			$endDate    = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal    = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Triwulan '.$nilai.' Tahun 2019';
			$strPeriode            = 'Triwulan '.$nilai;
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 3;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Triwulan '.$nilaiPembanding; 
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 1;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanAkhirPembanding = ($endMonth-3) > 1 ?  $endMonth-3 : 3;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Semester')
		{
			$startMonth = ($nilai == 1) ? 1 : 1;
			$endMonth   = ($nilai == 1) ? 6 : 6;
			$startMonth = ($nilai == 2) ? 7 : $startMonth;
			$endMonth   = ($nilai == 2) ? 12 : $endMonth;      
			$startDate = $tahun.'-'.$startMonth.'-'.'01';
			$endDate   = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal   = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Semester '.$nilai.' Tahun 2019';
			$strPeriode            = 'Semester '.$nilai;
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 1;
			$startDateCompare     = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 6;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Semester '.$nilaiPembanding; 
			$bulanAkhirPembanding = ($endMonth-6) > 1 ?  $endMonth-6 : 6;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Tahun')
		{
			$startDate = $tahun.'-01-01';
			$endDate   = $tahun.'-12-31';
			$tanggal = GetLastDateInMonth(12, $tahun);
			$strPeriodeHeader       = 'Tahun '.$tahun;
		                $strPeriode             = $strPeriodeHeader;//GetMonthName(12)." ".$nilai;
		                $tahunSebelumnya        = $tahun-1;
		                $startDateCompare       = $tahunSebelumnya.'-01-01';
		                $nilaiPembanding        = $tahun - 1;
		                $strPeriodePembanding   = 'Tahun '.$nilaiPembanding;
		                $endDateCompare         = $tahunSebelumnya.'-12-31';

		            } 

		            $this->load->model("M_laporan", "ModelLaporan");

		$data["saldoawal"]  = '';//$this->ModelLaporan->getSaldoAwalBukuBesar();

		$data["isitabel"] 	= $this->ModelLaporan->getLaporanNeraca($startDate, $endDate, $startDateCompare, $endDateCompare);

		$data["strPeriode"] = $strPeriode;

		$data["strPeriodeLaporan"] = $strPeriodeHeader;

		$content = $this->load->view('neraca', $data, true);

		echo $content;

	}

	public function showArusKas(){

		$periode       =  $this->uri->segment(3); 

		$nilai         =  $this->uri->segment(4);

		$tahunPeriode        =  $this->uri->segment(6);


		$startMonth = 1;

		$strPeriodeHeader = '';

		$strPeriode = '';

		$strPeriodePembanding = '';

		$tahun = $tahunPeriode;

		if ($periode == 'Bulan')
		{
			$startDate = $tahun.'-'.$nilai.'-'.'01';
			$endDate   = $tahun.'-'.$nilai.'-'.'31';
			$tanggal   = GetLastDateInMonth($nilai);
			$strPeriodeHeader     = $tanggal. " ".GetMonthName($nilai)." ".$tahun;
			$strPeriode           = GetMonthName($nilai)." ".$tahun;
			$bulanPembanding     = ($nilai-1 ) > 1 ?  $nilai-1 : 1;
			$strPeriodePembanding = GetMonthName($bulanPembanding)." ".$tahun;
			$bulanberikut         = $nilai+1;
			$strPeriodeBerikut = GetMonthName($bulanberikut)." ".$tahun;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$endDateCompare   = $tahun.'-'.$bulanPembanding.'-'.'31';
		}

		if ($periode == 'Triwulan')
		{
			$startMonth = ($nilai == 1) ? 1 : 0; 
			$endMonth   = ($nilai == 1) ? 3 : 0;
			$startMonth = ($nilai == 2) ? 4 : $startMonth;
			$endMonth   = ($nilai == 2) ? 6 : $endMonth;
			$startMonth = ($nilai == 3) ? 7 : $startMonth;
			$endMonth   = ($nilai == 3) ? 9 : $endMonth;
			$startMonth = ($nilai == 4) ? 10 : $startMonth;
			$endMonth   = ($nilai == 4) ? 12 : $endMonth;                 
			$startDate  = $tahun.'-'.$startMonth.'-'.'01';
			$endDate    = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal    = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Triwulan '.$nilai.' Tahun 2019';
			$strPeriode            = 'Triwulan '.$nilai;
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 3;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Triwulan '.$nilaiPembanding; 
			$bulanPembanding      = ($endMonth-5) > 1 ?  $endMonth-5 : 1;
			$startDateCompare = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanAkhirPembanding = ($endMonth-3) > 1 ?  $endMonth-3 : 3;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Semester')
		{
			$startMonth = ($nilai == 1) ? 1 : 1;
			$endMonth   = ($nilai == 1) ? 6 : 6;
			$startMonth = ($nilai == 2) ? 7 : $startMonth;
			$endMonth   = ($nilai == 2) ? 12 : $endMonth;      
			$startDate = $tahun.'-'.$startMonth.'-'.'01';
			$endDate   = $tahun.'-'.$endMonth.'-'.'31';
			$tanggal   = GetLastDateInMonth($endMonth);
			$strPeriodeHeader     = 'Semester '.$nilai.' Tahun 2019';
			$strPeriode            = 'Semester '.$nilai;
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 1;
			$startDateCompare     = $tahun.'-'.$bulanPembanding.'-'.'01';
			$bulanPembanding      = ($endMonth-12) > 1 ?  $endMonth-12 : 6;
			$nilaiPembanding      = ($nilai == 1) ?  $nilai : $nilai - 1;
			$strPeriodePembanding = 'Semester '.$nilaiPembanding; 
			$bulanAkhirPembanding = ($endMonth-6) > 1 ?  $endMonth-6 : 6;
			$endDateCompare   = $tahun.'-'.$bulanAkhirPembanding.'-'.'31';
		}

		if ($periode == 'Tahun')
		{
			$startDate = $tahun.'-01-01';
			$endDate   = $tahun.'-12-31';
			$tanggal = GetLastDateInMonth(12, $tahun);
			$strPeriodeHeader       = 'Tahun '.$tahun;
		                $strPeriode             = $strPeriodeHeader;//GetMonthName(12)." ".$nilai;
		                $tahunSebelumnya        = $tahun-1;
		                $startDateCompare       = $tahunSebelumnya.'-01-01';
		                $nilaiPembanding        = $tahun - 1;
		                $strPeriodePembanding   = 'Tahun '.$nilaiPembanding;
		                $endDateCompare         = $tahunSebelumnya.'-12-31';

		            } 

		            $this->load->model("M_laporan", "ModelLaporan");

		$data["saldoawal"]  = '';//$this->ModelLaporan->getSaldoAwalBukuBesar();

		$data["isitabel"] 	= $this->ModelLaporan->getLaporanArusKas($startDate, $endDate, $startDateCompare, $endDateCompare);

		$data["strPeriode"] = $strPeriode;

		$data["strPeriodeLaporan"] = $strPeriodeHeader;		

		$content = $this->load->view('aruskas', $data, true);

		echo $content;

	}

	function loadDataAkun(){

		$this->load->model("M_laporan", "ModelKodeAkun");

		echo $this->ModelKodeAkun->loadDataAkun();

	}

	function loadDataAkunCari(){

		$KataKunci = $this->input->post("data");

		$this->load->model("M_laporan", "ModelKodeAkun");

		echo $this->ModelKodeAkun->loadDataAkunCari($KataKunci);

	}


}





?>

