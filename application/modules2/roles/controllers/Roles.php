<?php 

/**
* 
*/
class Roles extends CI_controller
{

	function __construct()

	{

		parent::__construct();

		if (!$this->session->userdata("pengguna"))

		{

			redirect("Pengguna/login");

		}

		$this->load->model('M_roles');
		$this->load->model('user/M_user');

	}


	function index()

	{

		authorize('roles');
		$data['judul']="Daftar Roles";
		$data['act'] = 'roles';

		$data['roles']=$this->M_roles->tampil_roles();

		$this->themeadmin->tampilkan('tampilroles',$data);

	}

	function tambah()

	{
		authorize('roles');
		$data['judul']="Tambah Roles";

		$data['act'] = 'userRoles';

		$this->themeadmin->tampilkan('tambahroles',$data);

	}

	function simpan()
	{
		
		
		$datainputan=$this->input->post();

		$hasil  = $this->M_roles->simpanRoles($datainputan);

		if ($hasil=="gagal") 
		{
			$isipesan="<br><div class='alert alert-danger'>Roles Sudah Terdaftar, Gunakan Roles Lain!</div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect("Roles/tambah");
		}
		else
		{
			$isipesan="<br><div class='alert alert-success'>Data Roles Berhasil ditambah</div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect("Roles");
		}

	}

	function hapus($id)

	{

		$hasil = $this->M_roles->hapus_roles($id);

		if ($hasil=="gagal") 
		{
			$isipesan="<br><div class='alert alert-danger'>Data Gagal dihapus Karena Sedang digunakan!</div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect("Roles");
		}
		else
		{
			$isipesan="<br><div class='alert alert-success'>Data Berhasil dihapus</div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect("Roles");
		}

	}

	function ubah($id)

	{
		authorize('roles');
		$inputan=$this->input->post();

		if ($inputan) 

		{
			$this->M_roles->ubah_roles($inputan,$id);
			$isipesan="<br><div class='alert alert-success'>Data Berhasil Diubah </div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect('Roles');

		}

		$data['judul']="Ubah Roles";

		$data['admin']=$this->M_roles->ambil_roles($id);

		$this->themeadmin->tampilkan("ubahroles",$data);

	}

	function setting()
	{	
		authorize('setRoles');
		$data['id'] =  $this->input->get('id');

		$data['judul'] = 'Management Rules';
		$data['act'] = 'management';
		$data['roles'] =$this->M_user->getRoles();
		$this->themeadmin->tampilkan("settingroles",$data);
	}

	function simpanAkses()
	{
		$inputan =$this->input->post();

		$hasil = $this->M_roles->updateAkses($inputan);

		if ($hasil=='sukses') {
			$isipesan="<br><div class='alert alert-success'>Data Berhasil Disimpan </div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect('Roles/setting');
		} 
		

	}

}

?>