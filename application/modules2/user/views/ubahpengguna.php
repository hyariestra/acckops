<!-- data pelanggan ada di var $pelanggan -->

<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				<h3 class="box-title"><?php echo $judul ?></h3>

				<form method="post" enctype="multipart/form-data">

					<div class="box-body">

						<div class="form-group">

							<label>Username</label>

							<input type="text" name="email" class="form-control" value="<?php echo $admin['email']; ?>"></input>

							<label>Password</label>

							<input autocomplete="new-password" placeholder="kosongkan jika tidak ingin diubah.." type="password" name="password" class="form-control" value=""></input>

							<label>Nama</label>

							<input type="text" name="nama" class="form-control" value="<?php echo $admin['nama']; ?>"></input>

							<label>Role</label>

							<select class="form-control" name="role">

								<?php foreach ($roles as $key => $value): ?>

									<option value="<?php echo $value['id']; ?>" <?php if($value['id']==$admin['role']){echo"selected";} ?>><?php echo $value['name']; ?></option>

								<?php endforeach ?>

							</select>

							<label>Status</label>

							<select class="form-control" name="status">
								<option value="1" <?php if($admin['status'] == '1'){ echo 'selected'; } ?> >Aktif</option>
								<option value="0" <?php if($admin['status'] == '0'){ echo 'selected'; } ?> >Tidak Aktif</option>
							</select>

						</div>

						

					</div>

					<div class="box-footer">

						<button type="submit" class="btn btn-primary">Ubah</button>
						<a class="btn btn-default" href="<?php echo base_url("User")?>">Batal</a>
					</div>

				</form>

				

			</div>

			

		</div>

		

	</div>

	

</div>