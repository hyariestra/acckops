<!-- data pelanggan ada di var $pelanggan -->
<style type="text/css">
	.spanMargin{
		margin-right: 7px;
	
	}
	table, th, td{
		border: 1px solid #ddd !important;
	}
</style>
<div class="row">

	<div class="col-md-12">

		<div class="box">

			<div class="box-header">

				<h3 class="box-title"><?php echo $judul ?></h3>

			</div>

			<div class="box-body">
				<div class="row">

					<div style="margin-bottom: 20px" class="col-sm-3 col-sm-offset-9">
						<div class="input-group">
							<input placeholder="masukan pencarian..." id="InputKataKunci" name="InputKataKunci" type="text" class="form-control" aria-label="...">
							<div class="input-group-btn">
								<button type="button" onclick="cariAkun()" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="fa fa-search"></span>
								</button>
							</div><!-- /btn-group -->
						</div><!-- /input-group -->
					</div>
				</div>

				<table class="table table-striped table-bordered table-hover" id="tabelAkun">
					<thead style="background-color: #ecf0f1">
						<tr>
							<th>Kode</th>
							<th>Nama</th>
							<th>Saldo Normal</th>
							<th></th>
						</tr>
					</thead>
					
					<tbody name="tabelContent" id="tabelContent">
						
					</tbody>
				</table>



			</div>



		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      	<div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h4 class="modal-title">Tambah Akun</h4>
			        </div>
			        <div class="modal-body">
			        	<form id="formTambahAkun">
				        	<div class="form-horizontal">
				          		<div class="form-group">
								    <label class="col-sm-3 control-label" for="NamaPerusahaan">Arus Kas</label>
								    <div class="col-sm-8">
								    	<select class="form-control" name="ArusKas" id="ArusKas">
								    		<option value="0">:: Pilih Arus Kas ::</option>
								    		<?php
								    			$selectQuery = $this->db->query("SELECT * FROM ref_aruskas_kel");

								    			foreach ($selectQuery->result_array() as $key) {
								    		?>
								    		<option value="<?php echo $key['id_aruskas_kel']?>"><?php echo $key['kode'].' - '.$key['nama']?></option>
								    		<?php } ?>
								    	</select>
									</div>
							  	</div>
						  	</div>
						  	<div class="form-horizontal">
							  	<div class="form-group">
								    <label class="col-sm-3 control-label" for="KodeAkun">Kode Akun</label>
								    <div class="col-sm-8">
								    	<div class="input-group">
									    	<span class="input-group-addon" id="kodeAkunHeader"></span>
									    	<input type="text" class="form-control" id="KodeAkun" name="KodeAkun" aria-describedby="KodeAkun" placeholder="Masukan Kode Akun" value="" />
								    	</div>
									</div>
							  	</div>
						  	</div>
						  	<div class="form-horizontal">
							  	<div class="form-group">
								    <label class="col-sm-3 control-label" for="NamaAkun">Nama Akun</label>
								    <div class="col-sm-8">
								    	<input type="text" class="form-control" id="NamaAkun" name="NamaAkun" aria-describedby="NamaAkun" placeholder="Masukan Nama Akun" value="" />
									</div>
							  	</div>
						  	</div>
						  	<div class="form-horizontal">
				          		<div class="form-group">
								    <label class="col-sm-3 control-label" for="NamaPerusahaan">Saldo Normal</label>
								    <div class="col-sm-8">
								    	<select class="form-control" name="SaldoNormal" id="SaldoNormal">
								    		<option value="D">Debet (D)</option>
								    		<option value="K">Kredit (K)</option>
								    	</select>
									</div>
							  	</div>
						  	</div>
						  	<div class="form-horizontal">
							  	<div class="form-group">
								    <label class="col-sm-3 control-label" for="NamaAkun">Deskripsi</label>
								    <div class="col-sm-8">
								    	<textarea id="DeskripsiAkun" name="DeskripsiAkun" class="form-control"></textarea>
									</div>
							  	</div>
						  	</div>
					  	</form>
			        </div>
			        <div class="modal-footer">
			        	<input type="hidden" name="IDAkunSimpan" id="IDAkunSimpan" value="">
			        	<button type="button" class="btn btn-success" id="btnSimpan" onclick="SimpanTambahAkun(this)">Simpan</button>
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
		      	</div>
		    </div>
		</div>

		<!-- Modal Edit-->
		<div class="modal fade" id="ModalEdit" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      	<div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h4 class="modal-title">Edit Akun</h4>
			        </div>
			        <div class="modal-body">
			        	<form id="formEditAkun">
				        	<div class="form-horizontal">
				          		<div class="form-group">
								    <label class="col-sm-3 control-label" for="ArusKasEdit">Arus Kas</label>
								    <div class="col-sm-8">
								    	<select class="form-control" name="ArusKasEdit" id="ArusKasEdit">
								    		<option value="0">:: Pilih Arus Kas ::</option>
								    		<?php
								    			$selectQuery = $this->db->query("SELECT * FROM ref_aruskas_kel");

								    			foreach ($selectQuery->result_array() as $key) {
								    		?>
								    		<option value="<?php echo $key['id_aruskas_kel']?>"><?php echo $key['kode'].' - '.$key['nama']?></option>
								    		<?php } ?>
								    	</select>
									</div>
							  	</div>
						  	</div>
						  	<div class="form-horizontal">
							  	<div class="form-group">
								    <label class="col-sm-3 control-label" for="KodeAkunEdit">Kode Akun</label>
								    <div class="col-sm-8" id="tempelSpan">
								    	<div class="input-group">
									    	<span class="input-group-addon" id="kodeAkunHeaderEdit"></span>
									    	<input type="text" class="form-control" id="KodeAkunEdit" name="KodeAkunEdit" aria-describedby="KodeAkun" placeholder="Masukan Kode Akun" value="" />
								    	</div>
									</div>
							  	</div>
						  	</div>
						  	<div class="form-horizontal">
							  	<div class="form-group">
								    <label class="col-sm-3 control-label" for="NamaAkunEdit">Nama Akun</label>
								    <div class="col-sm-8">
								    	<input type="text" class="form-control" id="NamaAkunEdit" name="NamaAkunEdit" aria-describedby="NamaAkunEdit" placeholder="Masukan Nama Akun" value="" />
									</div>
							  	</div>
						  	</div>
						  	<div class="form-horizontal">
				          		<div class="form-group">
								    <label class="col-sm-3 control-label" for="SaldoNormalEdit">Saldo Normal</label>
								    <div class="col-sm-8">
								    	<select class="form-control" name="SaldoNormalEdit" id="SaldoNormalEdit">
								    		<option value="D">Debet (D)</option>
								    		<option value="K">Kredit (K)</option>
								    	</select>
									</div>
							  	</div>
						  	</div>
						  	<div class="form-horizontal">
							  	<div class="form-group">
								    <label class="col-sm-3 control-label" for="DeskripsiAkunEdit">Deskripsi</label>
								    <div class="col-sm-8">
								    	<textarea id="DeskripsiAkunEdit" name="DeskripsiAkunEdit" class="form-control"></textarea>
									</div>
							  	</div>
						  	</div>
					  	</form>
			        </div>
			        <div class="modal-footer">
			        	<input type="hidden" name="IDAkunSimpanEdit" id="IDAkunSimpanEdit" value="">
			        	<button type="button" class="btn btn-success" id="btnSimpanEdit" onclick="SimpanEditAkun(this)">Simpan</button>
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
		      	</div>
		    </div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('document').ready(function(){

		$("input[name='InputKataKunci']").on('keypress', function(e){
			if(e.which == 13)
			{
				//alert("oke");
				cariAkun();
			}
		});

		//var KataKunci = '';

		loadDataAkun();

    });

    function cariAkun(){

    	var KataKunci = $("#InputKataKunci").val();

    	if (KataKunci == '') {

    		$('#tabelContent').empty();

    		loadDataAkun();

    	}else{

    		loadDataAkunCari(KataKunci);

    	}

    }

    function loadDataAkunCari(Obj){

    	$('#tabelContent').empty();

    	var target = "<?php echo site_url("kodeakun/loadDataAkunCari")?>";
	            
	            var data = {

	            	data : Obj
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					for(i = 0; i < json.length; i++)
                    {

                        var idAkun 		   = json[i].idAkun,
                        	kodeWithFormat = json[i].kodeWithFormat,
                        	namaWithFormat = json[i].namaWithFormat,
                        	saldoNormal    = json[i].saldoNormal,
                        	concatAkun     = json[i].concatAkun,
                        	tombol         = json[i].tombol;

                        var table       = document.getElementById('tabelContent');  

                        var row         = table.insertRow();

                        row.id = idAkun;
                    
                        colKode         = row.insertCell(0);
                        colNama		    = row.insertCell(1);
                        colSaldo        = row.insertCell(2);
                        colAction       = row.insertCell(3);

                        colSaldo.style.textAlign        = 'center';


                        colKode.innerHTML           = kodeWithFormat;
                        colNama.innerHTML           = namaWithFormat;
                        colSaldo.innerHTML          = saldoNormal;
                        colAction.innerHTML         = '<input type="hidden" name="concatAkun" id="concatAkun" role="concatAkun" value="'+concatAkun+'"><input type="hidden" name="idAkun" tabFokus="'+idAkun+'" role="idAkun" id="idAkun" value="'+idAkun+'">'+tombol;
                        
                    }

				});

    }

    function loadDataAkun(){

    	var target = "<?php echo site_url("kodeakun/loadDataAkun")?>";
	            
	            var data = {
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					for(i = 0; i < json.length; i++)
                    {

                        var idAkun 		   = json[i].idAkun,
                        	kodeWithFormat = json[i].kodeWithFormat,
                        	namaWithFormat = json[i].namaWithFormat,
                        	saldoNormal    = json[i].saldoNormal,
                        	concatAkun     = json[i].concatAkun,
                        	tombol         = json[i].tombol;

                        var table       = document.getElementById('tabelContent');  

                        var row         = table.insertRow();

                        row.id = idAkun;
                    
                        colKode         = row.insertCell(0);
                        colNama		    = row.insertCell(1);
                        colSaldo        = row.insertCell(2);
                        colAction       = row.insertCell(3);

                        colSaldo.style.textAlign        = 'center';


                        colKode.innerHTML           = kodeWithFormat;
                        colNama.innerHTML           = namaWithFormat;
                        colSaldo.innerHTML          = saldoNormal;
                        colAction.innerHTML         = '<input type="hidden" name="concatAkun" id="concatAkun" role="concatAkun" value="'+concatAkun+'"><input type="hidden" name="idAkun" tabFokus="'+idAkun+'" role="idAkun" id="idAkun" value="'+idAkun+'">'+tombol;
                        
                    }

				});

    }

    function TambahAkun(Obj){

    	clearInput();

    	var concatAkun = $(Obj).closest('tr').find("input[role='concatAkun']").val();
    	var idAkun 	   = $(Obj).closest('tr').find("input[role='idAkun']").val();

    	$('#kodeAkunHeader').text(concatAkun);
    	$('#IDAkunSimpan').val(idAkun);

    	$('#myModal').modal('show');

    }

    function UbahAkun(Obj){

    	clearInput();

    	var idAkun 	   = $(Obj).closest('tr').find("input[role='idAkun']").val();

    	var target = "<?php echo site_url("kodeakun/AmbilDataAKun")?>";
	            
	            var data = {

	            		idAkun : idAkun
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					$('#ArusKasEdit').val(json.IDArusKas);
					$('#kodeAkunHeaderEdit').text(json.KodeInduk);
					$('#KodeAkunEdit').val(json.KodeAkun);
					$('#NamaAkunEdit').val(json.NamaAkun);
					$('#SaldoNormalEdit').val(json.SaldoNormal);
					$('#DeskripsiAkunEdit').val(json.Deskripsi);
					$('#IDAkunSimpanEdit').val(json.IDAkun);

					var strDisable = json.strDisable;

					if (strDisable == 'disable') {

						$('#spanDisable').remove();
						$('#KodeAkunEdit').removeAttr('readonly');

						$('#KodeAkunEdit').attr('readonly', 'true');
						$('#tempelSpan').append('<span id="spanDisable" style="color:red;">* Tidak dapat mengubah kode akun, Ada beberapa akun dibawahnya');

					}else{

						$('#KodeAkunEdit').removeAttr('readonly');
						$('#spanDisable').remove();
					}

				});

    	$('#ModalEdit').modal('show');

    }

    function HapusAkun(Obj){

    	if (confirm("Apakah Anda Yakin ?")) {

    		var idAkun 	   = $(Obj).closest('tr').find("input[role='idAkun']").val();

    		var target = "<?php echo site_url("kodeakun/HapusAkun")?>";
	            
	            var data = {

	            		idAkun : idAkun
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					var hasil = json.hasil;

					if (hasil == 'berhasil') {

						alert(json.pesan);

						$("#tabelContent").empty();

						loadDataAkun();

					}else{

						alert(json.pesan);

					}
				});
		    
		}

    }

    function SimpanTambahAkun(Obj){

    	var idAkun = $(Obj).prev().val();

    	var target = "<?php echo site_url("kodeakun/TambahAkun")?>";

    	var formSerialize   = $("#formTambahAkun").serialize();
	            
	            var data = {

	            		data   : formSerialize,
	            		idAkun : idAkun
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					var hasil = json.hasil;

					if (hasil == 'berhasil') {

						alert(json.pesan);

						$("#tabelContent").empty();

						loadDataAkun();

						$('#myModal').modal('hide');

						clearInput();

					}else{

						alert(json.pesan);

					}
				});

    }

    function SimpanEditAkun(Obj){

    	var idAkun = $(Obj).prev().val();

    	var target = "<?php echo site_url("kodeakun/UbahAkun")?>";

    	var formSerialize   = $("#formEditAkun").serialize();
	            
	            var data = {

	            		data   : formSerialize,
	            		idAkun : idAkun
	            }

	            $.post(target, data, function(e){

	            	//console.log(e);
				
					var json = $.parseJSON(e);

					var hasil = json.hasil;

					if (hasil == 'berhasil') {

						alert(json.pesan);

						$("#tabelContent").empty();

						loadDataAkun();

						$('#ModalEdit').modal('hide');

						clearInput();

					}else{

						alert(json.pesan);

					}
				});

    }

    function clearInput(){

    	$('#ArusKasEdit').val(0);
		$('#kodeAkunHeaderEdit').text('');
		$('#KodeAkunEdit').val('');
		$('#NamaAkunEdit').val('');
		$('#SaldoNormalEdit').val(0);
		$('#DeskripsiAkunEdit').val('');
		$('#IDAkunSimpanEdit').val('');

		$('#ArusKas').val(0);
		$('#kodeAkunHeader').text('');
		$('#KodeAkun').val('');
		$('#NamaAkun').val('');
		$('#SaldoNormal').val(0);
		$('#DeskripsiAkun').val('');
		$('#IDAkunSimpan').val('');

    }
</script>