<?php 


function getInfo($v)

{
	$ci =& get_instance();


	$val = $ci->db->query("SELECT ".$v." FROM mst_informasi")->first_row();

	return  $val->$v;

}

function compareMenu($menu)
{


	$query = authQuery();

	foreach ($query as $key => $value) {
		$val[] = $value['menu'];
	}

	$count  = count(array_intersect($menu, $val));

	$count = $count==0 ? 'none':'';

	return $count;

}

function menuAuth($menu)
{
	$query = authQuery();

	foreach ($query as $key => $value) {
		$val[] = $value['menu'];
	}

	$val['condition'] = in_array($menu, $val)?true:false;

	


	return $val;
}


function authorize($menu)
{
	

	$query = authQuery();

	foreach ($query as $key => $value) {
		$val[] = $value['menu'];
	}


	$val = in_array($menu, $val)?true:false;
	
	if ($val==false) {
		redirect("Dashboard/notFound");
	} 
	
}

function checkedHelp($id,$menu)
{
	$ci =& get_instance();

	$ci->db->select('*')
	->from('akses_menu')->where('id_role', $id);
	$ambil = $ci->db->get();

	$query=$ambil->result_array();

	if ($query==null) {
		$val = '';
	} else {
		foreach ($query as $key => $value) {
			$val[] = $value['menu'];
		}


		$val = in_array($menu, $val)?'checked':'';
	}
	



	return $val;
}


function authQuery()
{
	$ci =& get_instance();

	$idRole = $ci->session->userdata['pengguna']['role'];
	
	$q =  $ci->db->query("SELECT *
		FROM admin 
		LEFT JOIN roles ON roles.id = admin.role
		LEFT JOIN akses_menu on akses_menu.id_role = roles.id
		WHERE admin.role = ".$idRole." ")->result_array();

	return $q;

}


function panggil_file($namafile)

{

	//mengakses core dari codeingiter

	$ci =& get_instance();

	return $ci->load->view($namafile);

}



function tampil_pengaturan($namakolom)

{

	$ci =& get_instance();

	$ci->db->where("kolom",$namakolom);

	$ambil=$ci->db->get("pengaturan");

	$pecah=$ambil->row_array();

	return $pecah['isi'];

}

function tampil_halaman($kolom)

{

	$ci =& get_instance();

	$ci->db->where("judul",$kolom);

	$ambil=$ci->db->get("halamanstatis");

	$pecah=$ambil->row_array();

	return $pecah['judul'];

}


?>