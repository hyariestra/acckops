-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2019 at 11:13 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `accounting_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `password`, `status`, `nama`, `role`) VALUES
(1, 'superadmin', '827ccb0eea8a706c4c34a16891f84e7b', 1, 'yudhazzz', 1),
(7, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 1, 'admin y11222', 3);

-- --------------------------------------------------------

--
-- Table structure for table `akses_menu`
--

CREATE TABLE `akses_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(100) NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akses_menu`
--

INSERT INTO `akses_menu` (`id`, `menu`, `id_role`) VALUES
(118, 'informasiPerusahaan', 1),
(119, 'kodeAkun', 1),
(120, 'penandatanganan', 1),
(121, 'konfigurasi', 1),
(122, 'saldoAwal', 1),
(123, 'jurnal', 1),
(124, 'jurnalumum', 1),
(125, 'bukubesar', 1),
(126, 'labarugi', 1),
(127, 'ekuitas', 1),
(128, 'neraca', 1),
(129, 'aruskas', 1),
(130, 'pengguna', 1),
(131, 'roles', 1),
(132, 'setRoles', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_akun`
--

CREATE TABLE `mst_akun` (
  `id_akun` int(10) NOT NULL,
  `id_aruskas_kel` int(2) DEFAULT '0',
  `kode_akun` varchar(20) DEFAULT NULL,
  `nama_akun` varchar(200) DEFAULT NULL,
  `id_induk` int(10) DEFAULT '0',
  `kode_induk` varchar(20) DEFAULT NULL,
  `level` int(2) DEFAULT '0',
  `header` int(1) DEFAULT '0',
  `saldo_normal` varchar(1) DEFAULT NULL,
  `deskripsi` varchar(254) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_akun`
--

INSERT INTO `mst_akun` (`id_akun`, `id_aruskas_kel`, `kode_akun`, `nama_akun`, `id_induk`, `kode_induk`, `level`, `header`, `saldo_normal`, `deskripsi`, `date_entry`) VALUES
(1, 0, '1', 'ASET', 0, '0', 1, 1, 'D', '', '0000-00-00 00:00:00'),
(2, 0, '1', 'ASET LANCAR', 1, '1', 2, 1, 'D', '', '0000-00-00 00:00:00'),
(3, 1, '1', 'Kas dan Setara Kas', 2, '1.1', 3, 1, 'D', '', '0000-00-00 00:00:00'),
(10, 2, '2', 'Investasi Jangka Pendek', 2, '1.1', 3, 1, 'D', '', '0000-00-00 00:00:00'),
(15, 1, '3', 'Piutang Usaha', 2, '1.1', 3, 1, 'D', '', '0000-00-00 00:00:00'),
(27, 0, '4', 'Piutang Lainnya', 2, '1.1', 3, 1, 'D', '', '0000-00-00 00:00:00'),
(29, 0, '5', 'Cadangan Kerugian Piutang', 2, '1.1', 3, 1, 'D', '', '0000-00-00 00:00:00'),
(32, 0, '3', 'ASET TETAP', 1, '1', 2, 1, 'D', '', '0000-00-00 00:00:00'),
(45, 0, '4', 'ASET LAIN-LAIN', 1, '1', 2, 1, 'D', '', '0000-00-00 00:00:00'),
(46, 2, '1', 'Aset Tetap Rusak Berat', 45, '1.4', 3, 1, 'D', '', '0000-00-00 00:00:00'),
(47, 1, '2', 'Persediaan Kadaluarsa', 45, '1.4', 3, 1, 'D', '', '0000-00-00 00:00:00'),
(49, 0, '2', 'KEWAJIBAN', 0, '0', 1, 1, 'K', '', '0000-00-00 00:00:00'),
(50, 0, '1', 'KEWAJIBAN JANGKA PENDEK', 49, '2', 2, 1, 'K', '', '0000-00-00 00:00:00'),
(51, 3, '1', 'Hutang Usaha', 50, '2.1', 3, 1, 'K', '', '0000-00-00 00:00:00'),
(52, 1, '2', 'Utang Bunga', 50, '2.1', 3, 0, 'K', '', '0000-00-00 00:00:00'),
(53, 3, '3', 'Bagian Lancar Utang Jangka Panjang', 50, '2.1', 3, 0, 'K', '', '0000-00-00 00:00:00'),
(54, 1, '4', 'Utang  Pajak', 50, '2.1', 3, 1, 'K', '', '0000-00-00 00:00:00'),
(55, 1, '5', 'Biaya Yang Masih Harus Dibayar', 50, '2.1', 3, 1, 'K', '', '0000-00-00 00:00:00'),
(56, 3, '6', 'Pendapatan Diterima Dimuka', 50, '2.1', 3, 1, 'K', '', '0000-00-00 00:00:00'),
(60, 0, '2', 'KEWAJIBAN JANGKA PANJANG', 49, '2', 2, 1, 'K', '', '0000-00-00 00:00:00'),
(61, 3, '1', 'Utang Bank', 60, '2.2', 3, 0, 'K', '', '0000-00-00 00:00:00'),
(62, 3, '2', 'Utang Imbalan Kerja Pegawai', 60, '2.2', 3, 0, 'K', '', '0000-00-00 00:00:00'),
(67, 0, '3', 'EKUITAS', 0, '0', 1, 1, 'K', '', '0000-00-00 00:00:00'),
(82, 0, '1', 'EKUITAS', 67, '3', 2, 1, 'K', '', '0000-00-00 00:00:00'),
(83, 1, '1', 'Ekuitas', 82, '3.1', 3, 1, 'K', '', '0000-00-00 00:00:00'),
(86, 1, '4', 'PENDAPATAN', 0, '0', 1, 1, 'K', '', '0000-00-00 00:00:00'),
(87, 1, '1', 'PENDAPATAN BLUD', 86, '4', 2, 1, 'K', '', '0000-00-00 00:00:00'),
(88, 0, '1', 'Jasa Layanan', 87, '4.1', 3, 1, 'K', '', '0000-00-00 00:00:00'),
(91, 0, '2', 'Hibah', 87, '4.1', 3, 1, 'K', '', '0000-00-00 00:00:00'),
(106, 1, '5', 'BIAYA OPERASIONAL PELAYANAN', 0, '0', 1, 1, 'D', '', '0000-00-00 00:00:00'),
(123, 1, '6', 'BIAYA OPERASIONAL ADMINISTRASI DAN UMUM', 0, '0', 1, 1, 'D', '', '0000-00-00 00:00:00'),
(189, 1, '8', 'PENDAPATAN LAIN-LAIN', 0, '0', 1, 1, 'K', '', '0000-00-00 00:00:00'),
(194, 1, '9', 'BIAYA NON OPERASIONAL', 0, '0', 1, 1, 'D', '', '0000-00-00 00:00:00'),
(213, 0, '3', 'Hasil Kerjasama', 87, '4.1', 3, 1, 'K', '', '2015-04-16 14:04:39'),
(214, 0, '4', 'Lain lain Pendapatan BLUD yang Sah', 87, '4.1', 3, 1, 'K', '', '2015-04-16 14:04:53'),
(217, 1, '2', 'PENDAPATAN/PENERIMAAN APBD', 86, '4', 2, 1, 'K', '', '2015-04-16 14:07:56'),
(218, 1, '3', 'PENDAPATAN / PENERIMAAN APBN', 86, '4', 2, 1, 'K', '', '2015-04-16 14:08:16'),
(219, 0, '1', 'Pendapatan/Penerimaan APBD', 217, '4.2', 3, 1, 'K', '', '2015-04-16 14:08:49'),
(220, 0, '1', 'Pendapatan/Penerimaan APBN', 218, '4.3', 3, 1, 'K', '', '2015-04-16 14:09:12'),
(228, 0, '1', 'Biaya Pegawai', 561, '6.1', 3, 1, 'D', '', '2015-04-16 14:58:26'),
(229, 0, '2', 'Biaya Administrasi Kantor', 561, '6.1', 3, 1, 'D', '', '2015-04-16 14:59:04'),
(230, 0, '3', 'Biaya Pemeliharaan Adm dan UM', 561, '6.1', 3, 1, 'D', '', '2015-04-16 14:59:21'),
(231, 0, '4', 'Biaya Barang dan Jasa', 561, '6.1', 3, 1, 'D', '', '2015-04-16 14:59:53'),
(232, 0, '5', 'Biaya Promosi', 561, '6.1', 3, 1, 'D', '', '2015-04-16 15:00:41'),
(233, 0, '6', 'Biaya Penyusutan & Amortisasi', 561, '6.1', 3, 1, 'D', '', '2015-04-16 15:00:55'),
(234, 0, '7', 'Biaya Administrasi Umum dan Lain-Lain', 561, '6.1', 3, 1, 'D', '', '2015-04-16 15:01:40'),
(235, 1, '1', 'BIAYA NON OPERASIONAL', 194, '9', 2, 1, 'D', '', '2015-04-16 15:02:24'),
(240, 0, '1', 'Biaya Pegawai', 565, '5.1', 3, 1, 'D', '', '2015-04-16 15:05:57'),
(241, 0, '2', 'Biaya Bahan', 565, '5.1', 3, 1, 'D', '', '2015-04-16 15:06:15'),
(242, 0, '3', 'Biaya Jasa Pelayanan', 565, '5.1', 3, 1, 'D', '', '2015-04-16 15:06:31'),
(243, 0, '4', 'Biaya Pemeliharaan', 565, '5.1', 3, 1, 'D', '', '2015-04-16 15:06:53'),
(244, 0, '5', 'Biaya Barang dan Jasa', 565, '5.1', 3, 1, 'D', '', '2015-04-16 15:07:17'),
(245, 1, '6', 'Biaya Penyusutan', 565, '5.1', 3, 0, 'D', '', '2015-04-16 15:07:31'),
(246, 0, '7', 'Biaya Pelayanan Lain-lain', 565, '5.1', 3, 1, 'D', '', '2015-04-16 15:07:55'),
(247, 1, '1', 'PENDAPATAN LAIN-LAIN', 189, '8', 2, 1, 'K', '', '2015-04-16 15:14:12'),
(500, 0, '7', 'BIAYA INVESTASI', 0, '0', 1, 1, 'D', '', '0000-00-00 00:00:00'),
(501, 0, '3', 'BIAYA INVESTASI', 500, '7', 2, 1, 'D', '', '2015-10-16 16:56:10'),
(548, 0, '6', 'Biaya dibayar dimuka', 2, '1.1', 3, 1, 'D', '', '2015-10-23 13:41:48'),
(549, 0, '7', 'Persediaan', 2, '1.1', 3, 1, 'D', '', '2015-10-23 13:42:05'),
(550, 0, '2', 'INVESTASI JANGKA PANJANG', 1, '1', 2, 1, 'D', '', '2015-10-23 13:43:18'),
(551, 2, '1', 'Investasi Jangka Panjang Non Permanen', 550, '1.2', 3, 0, 'D', '', '2015-10-23 13:46:16'),
(552, 2, '2', 'Investasi Jangka Panjang Permanen', 550, '1.2', 3, 0, 'D', '', '2015-10-23 13:46:37'),
(554, 0, '99', 'Akumulasi Penyusutan', 32, '1.3', 3, 1, 'D', '', '2015-10-23 13:52:56'),
(555, 0, '4', 'Konstruksi dalam Pengerjaan', 45, '1.4', 3, 1, 'D', '', '2015-10-23 13:56:49'),
(561, 1, '1', 'BIAYA ADMINISTRASI DAN UMUM', 123, '6', 2, 1, 'D', '', '2015-10-23 14:45:15'),
(565, 1, '1', 'BIAYA PELAYANAN', 106, '5', 2, 1, 'D', '', '2015-10-23 14:57:07'),
(574, 0, '1', 'Pendapatan Bunga Bank', 247, '8.1', 3, 1, 'D', '', '2015-10-23 15:12:28'),
(575, 0, '2', 'Laba Penjualan Aset Tetap', 247, '8.1', 3, 1, 'D', '', '2015-10-23 15:12:47'),
(576, 0, '3', 'Denda', 247, '8.1', 3, 1, 'D', '', '2015-10-23 15:13:01'),
(577, 1, '1', 'Biaya Bunga', 235, '9.1', 3, 1, 'D', '', '2015-10-23 15:14:04'),
(578, 0, '2', 'Biaya Administrasi Bank', 235, '9.1', 3, 1, 'D', '', '2015-10-23 15:15:56'),
(579, 1, '3', 'Biaya Kerugian Penjualan Aset Tetap', 235, '9.1', 3, 0, 'D', '', '2015-10-23 15:16:58'),
(580, 1, '4', 'Biaya Kerugian Penurunan Nilai', 235, '9.1', 3, 0, 'D', '', '2015-10-23 15:17:30'),
(780, 2, '1', 'Tanah', 32, '1.3', 3, 1, 'D', '', '2015-11-07 09:08:10'),
(781, 0, '1', 'Tanah', 501, '7.3', 3, 1, 'D', '', '2015-11-07 09:08:10'),
(783, 2, '2', 'Gedung dan Bangunan', 32, '1.3', 3, 1, 'D', '', '2015-11-07 09:08:39'),
(784, 0, '2', 'Gedung dan Bangunan', 501, '7.3', 3, 1, 'D', '', '2015-11-07 09:08:39'),
(786, 2, '3', 'Peralatan dan Mesin', 32, '1.3', 3, 1, 'D', '', '2015-11-07 09:08:58'),
(787, 0, '3', 'Peralatan dan Mesin', 501, '7.3', 3, 1, 'D', '', '2015-11-07 09:08:58'),
(789, 2, '4', 'Kendaraan', 32, '1.3', 3, 1, 'D', '', '2015-11-07 09:09:11'),
(790, 0, '4', 'Kendaraan', 501, '7.3', 3, 1, 'D', '', '2015-11-07 09:09:11'),
(792, 2, '5', 'Perlengkapan dan Peralatan Kantor', 32, '1.3', 3, 1, 'D', '', '2015-11-07 09:09:30'),
(793, 0, '5', 'Perlengkapan dan Peralatan Kantor', 501, '7.3', 3, 1, 'D', '', '2015-11-07 09:09:30'),
(795, 2, '6', 'Jalan, Irigasi dan Jaringan', 32, '1.3', 3, 1, 'D', '', '2015-11-07 09:10:01'),
(796, 0, '6', 'Jalan, Irigasi dan Jaringan', 501, '7.3', 3, 1, 'D', '', '2015-11-07 09:10:01'),
(832, 1, '9', 'Sisa Lebih Penggunaan Anggaran (SiLPA)', 86, '4', 2, 1, 'K', '', '2016-01-12 09:43:27'),
(833, 0, '1', 'Sisa Lebih Penggunaan Anggaran (SiLPA)', 832, '4.9', 3, 1, 'K', '', '2016-01-12 09:43:43'),
(1027, 0, '7', 'Aset Tetap Lainnya', 32, '1.3', 3, 1, 'D', '', '2016-10-01 14:38:46'),
(1028, 0, '7', 'Aset Tetap Lainnya', 501, '7.3', 3, 1, 'D', '', '2016-10-01 14:38:46'),
(1265, 1, '06', 'Biaya Pajak Bunga', 235, '9.1', 3, 1, 'D', '', '2017-08-01 11:42:41'),
(1277, 2, '5', 'Aset Lain-Lain', 45, '1.4', 3, 1, 'D', '', '2017-08-04 11:28:08'),
(1281, 2, '6', 'Akumulasi Amortisasi Aset Tak Berwujud', 45, '1.4', 3, 1, 'D', '', '2017-08-30 14:12:28'),
(1282, 2, '7', 'Akumulasi Penyusutan Aset Lain-Lain', 45, '1.4', 3, 1, 'D', '', '2017-08-30 14:13:01'),
(1290, 2, '8', 'Aset Lain Lainnya', 32, '1.3', 3, 1, 'D', '', '2018-10-21 10:36:33'),
(1291, 2, '8', 'Aset Lain Lainnya', 501, '7.3', 3, 1, 'D', '', '2018-10-21 10:36:33');

-- --------------------------------------------------------

--
-- Table structure for table `mst_informasi`
--

CREATE TABLE `mst_informasi` (
  `id_info` int(5) NOT NULL,
  `nama_perusahaan` varchar(100) DEFAULT NULL,
  `logo_perusahaan` varchar(500) DEFAULT NULL,
  `alamat` varchar(1000) DEFAULT NULL,
  `kode_pos` char(10) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nomor_telp` int(15) DEFAULT NULL,
  `fax` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_informasi`
--

INSERT INTO `mst_informasi` (`id_info`, `nama_perusahaan`, `logo_perusahaan`, `alamat`, `kode_pos`, `website`, `email`, `nomor_telp`, `fax`) VALUES
(1, 'dasdsa', 'logo-umy.png', 'as', 'dasdsa', 'dsadsad', 'asdsad@gmail.com', 23, 32);

-- --------------------------------------------------------

--
-- Table structure for table `penandatanganan`
--

CREATE TABLE `penandatanganan` (
  `id` int(11) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(200) NOT NULL,
  `tipe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penandatanganan`
--

INSERT INTO `penandatanganan` (`id`, `jabatan`, `nama`, `nip`, `tipe`) VALUES
(7, 'ASN12', 'tes', '43284324', 1),
(8, 'dasdas', 'dasdas', '99000', 2),
(9, 'dasdas', 'dasdas', 'dasd', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id_pengaturan` int(11) NOT NULL,
  `kolom` varchar(100) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id_pengaturan`, `kolom`, `isi`) VALUES
(1, 'judul_seo', 'Moerop Sticker - Aksesoris Motor'),
(2, 'keyword', 'Sticker Motor Moeroep'),
(3, 'nama_web', 'Koperasi UMY'),
(4, 'tagline', 'kerajinan sticker '),
(5, 'alamat_usaha', 'Jln, Jendral Sudirman, no.139 tidar selatan, magelang selatan, kota magelang, jawa tengah'),
(6, 'telepon_usaha', '082199636311'),
(7, 'email_usaha', 'moeroepsticker@gmail.com'),
(8, 'tentang_footer', 'desain sticker'),
(9, 'nama_usaha', 'Koperasi UMY'),
(10, 'rek_usaha', 'Mandiri acc: 7880-7505-04<br />\r\nRiswan Afandy<br />\r\nCabang: Magelang'),
(11, 'logo_web', '<img alt=\"\" class=\"img-responsive\" src=\"http://localhost/trainit_ayud/aplikasi/assets/upload/images/Untitled-1(1).png\" style=\"height:147px; margin-left:40px; margin-right:40px; width:150px\" />'),
(12, 'ig', 'moeroep');

-- --------------------------------------------------------

--
-- Table structure for table `ref_aruskas_kel`
--

CREATE TABLE `ref_aruskas_kel` (
  `id_aruskas_kel` int(1) NOT NULL,
  `kode` varchar(5) DEFAULT NULL,
  `nama` varchar(20) DEFAULT NULL,
  `deskripsi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_aruskas_kel`
--

INSERT INTO `ref_aruskas_kel` (`id_aruskas_kel`, `kode`, `nama`, `deskripsi`) VALUES
(1, '1', 'Operasional', NULL),
(2, '2', 'Investasi', NULL),
(3, '3', 'Pendanaan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_bulan`
--

CREATE TABLE `ref_bulan` (
  `id_bulan` int(2) NOT NULL,
  `nama_bulan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_bulan`
--

INSERT INTO `ref_bulan` (`id_bulan`, `nama_bulan`) VALUES
(1, 'Januari'),
(2, 'Februari'),
(3, 'Maret'),
(4, 'April'),
(5, 'Mei'),
(6, 'Juni'),
(7, 'Juli'),
(8, 'Agustus'),
(9, 'September'),
(10, 'Oktober'),
(11, 'Nopember'),
(12, 'Desember');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jenis_trans`
--

CREATE TABLE `ref_jenis_trans` (
  `id_jenis_trans` int(2) NOT NULL,
  `kode` varchar(2) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jenis_trans`
--

INSERT INTO `ref_jenis_trans` (`id_jenis_trans`, `kode`, `nama`) VALUES
(1, 'JU', 'Jurnal Umum'),
(2, 'SA', 'Saldo Awal');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'superadmin'),
(3, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `set_setting`
--

CREATE TABLE `set_setting` (
  `id_setting` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `nilai` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `set_setting`
--

INSERT INTO `set_setting` (`id_setting`, `nama`, `nilai`) VALUES
(3, 'akun_pajak', '07.03');

-- --------------------------------------------------------

--
-- Table structure for table `trx_ju`
--

CREATE TABLE `trx_ju` (
  `id_ju` int(20) NOT NULL,
  `id_jenis_trans` int(1) DEFAULT NULL,
  `nomor` varchar(100) DEFAULT NULL,
  `uraian` varchar(300) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_ju`
--

INSERT INTO `trx_ju` (`id_ju`, `id_jenis_trans`, `nomor`, `uraian`, `tanggal`, `date_entry`) VALUES
(34, 1, 'JU00001', 'ewfwef', '2019-05-06', '2019-05-06 22:10:56'),
(35, 1, 'JU00002', 'dsadas', '2018-02-02', '2019-05-23 21:14:36'),
(36, 1, 'JU00003', 'sdsadsa', '2019-05-23', '2019-05-23 21:14:59'),
(37, 1, 'JU00004', 'dasdasdas', '2019-05-23', '2019-05-23 23:51:36');

-- --------------------------------------------------------

--
-- Table structure for table `trx_judet`
--

CREATE TABLE `trx_judet` (
  `id_judet` int(20) NOT NULL,
  `id_ju` int(10) DEFAULT '0',
  `id_akun` int(5) DEFAULT '0',
  `memo` varchar(200) DEFAULT NULL,
  `debet` decimal(25,4) DEFAULT '0.0000',
  `kredit` decimal(25,4) DEFAULT '0.0000',
  `date_entry` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_judet`
--

INSERT INTO `trx_judet` (`id_judet`, `id_ju`, `id_akun`, `memo`, `debet`, `kredit`, `date_entry`) VALUES
(57, 34, 551, 'dsad', '90000.0000', '0.0000', '2019-05-06 22:10:56'),
(58, 34, 552, 'dsadsa', '0.0000', '90000.0000', '2019-05-06 22:10:56'),
(65, 35, 551, 'sdad', '5444.0000', '0.0000', '2019-05-23 21:14:36'),
(66, 35, 552, 'dsa', '0.0000', '5444.0000', '2019-05-23 21:14:36'),
(67, 36, 551, '', '45444.0000', '0.0000', '2019-05-23 21:14:59'),
(68, 36, 62, '', '0.0000', '45444.0000', '2019-05-23 21:14:59'),
(72, 37, 551, '', '311100.0000', '0.0000', '2019-05-23 23:51:36'),
(73, 37, 62, '', '0.0000', '200000.0000', '2019-05-23 23:51:36'),
(74, 37, 551, '', '0.0000', '111100.0000', '2019-05-23 23:51:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `akses_menu`
--
ALTER TABLE `akses_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_akun`
--
ALTER TABLE `mst_akun`
  ADD PRIMARY KEY (`id_akun`),
  ADD KEY `kode_akun` (`kode_akun`,`nama_akun`,`id_induk`,`kode_induk`,`level`,`header`);

--
-- Indexes for table `mst_informasi`
--
ALTER TABLE `mst_informasi`
  ADD PRIMARY KEY (`id_info`);

--
-- Indexes for table `penandatanganan`
--
ALTER TABLE `penandatanganan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id_pengaturan`);

--
-- Indexes for table `ref_aruskas_kel`
--
ALTER TABLE `ref_aruskas_kel`
  ADD PRIMARY KEY (`id_aruskas_kel`),
  ADD KEY `kode` (`kode`,`nama`);

--
-- Indexes for table `ref_bulan`
--
ALTER TABLE `ref_bulan`
  ADD PRIMARY KEY (`id_bulan`);

--
-- Indexes for table `ref_jenis_trans`
--
ALTER TABLE `ref_jenis_trans`
  ADD PRIMARY KEY (`id_jenis_trans`),
  ADD KEY `id_akunkel` (`id_jenis_trans`,`kode`,`nama`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `set_setting`
--
ALTER TABLE `set_setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `trx_ju`
--
ALTER TABLE `trx_ju`
  ADD PRIMARY KEY (`id_ju`),
  ADD UNIQUE KEY `nomor` (`nomor`),
  ADD KEY `id_unit_kerja` (`nomor`,`uraian`,`tanggal`);

--
-- Indexes for table `trx_judet`
--
ALTER TABLE `trx_judet`
  ADD PRIMARY KEY (`id_judet`),
  ADD KEY `id_ju` (`id_ju`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `akses_menu`
--
ALTER TABLE `akses_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `mst_akun`
--
ALTER TABLE `mst_akun`
  MODIFY `id_akun` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1292;

--
-- AUTO_INCREMENT for table `mst_informasi`
--
ALTER TABLE `mst_informasi`
  MODIFY `id_info` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `penandatanganan`
--
ALTER TABLE `penandatanganan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id_pengaturan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ref_aruskas_kel`
--
ALTER TABLE `ref_aruskas_kel`
  MODIFY `id_aruskas_kel` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ref_bulan`
--
ALTER TABLE `ref_bulan`
  MODIFY `id_bulan` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ref_jenis_trans`
--
ALTER TABLE `ref_jenis_trans`
  MODIFY `id_jenis_trans` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `set_setting`
--
ALTER TABLE `set_setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `trx_ju`
--
ALTER TABLE `trx_ju`
  MODIFY `id_ju` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `trx_judet`
--
ALTER TABLE `trx_judet`
  MODIFY `id_judet` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
